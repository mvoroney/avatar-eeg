#!/usr/bin/env bash

basedir=$(dirname $(dirname `readlink $0 || echo $PWD/$0`))

source "${basedir}/Scripts/utils.sh"

preamble="${basedir}/licence.preamble"

for file in $*; do
    prepend-block $file '/*' $preamble '*/'
done
