
#
# Quotes regular expression metacharacters. Outputs the transformed string.
#
# \param line the line to quote.
#
function quote_meta {
    echo $1 | sed 's/\\/\\\\/g; s/\//\\\//g'
}

#
# Determines whether or not a file contains a block of text.
# Issues no output
#
# \param file the name of the file to check
# \param block the anme of a file containing the block of text to search for.
#
# \return 0 if the block was found, 1 otherwise.
#
function contains-block {
    local file=$1;
    local blockf=$2;
    shift 2

    local block_start=`head -n 1 $blockf`
    local block_end=`tail -n 1 $blockf`

    local t=$(mktemp /tmp/utils.contains_block.XXX)

    sed -n "\@$block_start@,\@$block_end@ p" $file > $t
    diff -q $t $blockf > /dev/null 2>&1
    local status=$?
    
    rm -f -- $t

    return $status
}

#
# Prepends the contents of one file to another, encapsulated by pre and post.
#
# \param file the name of the file to modify.
# \param pre the pre text to insert immediately before the text block.
# \param block the name of the file containing the block to prepend.
# \param post the post text to insert immediately following the text block.
#
function prepend-block {
    local file=$1
    local pre=$2
    local blockf=$3
    local post=$4
    shift 4

    contains-block $file $blockf
    if [ $? -ne 0 ]; then
        local t=$(mktemp /tmp/utils.prepend_block.XXX)
       
        # build the new file
        echo "$pre" > $t
        cat $blockf >> $t
        echo "$post" >> $t
        cat $file >> $t

        # swap with original
        mv -f $t $file
    fi

}


#
# Downloads and mounts the requested .dmg archive using curl
#
# \param src_dir The directory to download the .dmg archive to.
# \param mount_root The base directory where the archive will be mounted.
#   The mount point will be ${mount_root}/${dep_name}
# \param dep_name The name of the dependency to download. The archive
#   download url is expected to be: ${dep_url}/${dep_name}-${dep-version}.dmg
# \param dep_version The version string for the archive to download.
# \param dep_url The base url where the archive will be downloaded from.
#
function install_dmg {

    local src_dir=$1
    local mount_root=$2
    local dep_name=$3
    local dep_version=$4
    local dep_url=$5
    
    local status=0

    # download the archive if it doesn't exist yet.
    if [ ! -f "${src_dir}/${dep_name}.dmg" ]; then

        echo "${dep_name} not found, downloading from ${dep_url}..."
        curl -L -o "${src_dir}/${dep_name}.dmg" "${dep_url}/${dep_name}-${dep_version}.dmg"
        status=$?
    
    fi

    # make the mountpoint directory if it doesn't exist.
    if [ $status -a ! -d "${mount_root}/${dep_name}" ]; then

        echo "${dep_name} mount directory not found, creating ${mount_root}/${dep_name}..."
        mkdir "${mount_root}/${dep_name}"
        status=$?

    fi

    # if the mountpoint is empty, it's probably not mounted.
    if [ $status -a 0 -eq `ls "${mount_root}/${dep_name}" | wc -l` ]; then

        echo "${dep_name} package not mounted. mounting at ${mount_root}/${dep_name}..."
        hdiutil attach "${src_dir}/${dep_name}.dmg" -mountpoint "${mount_root}/${dep_name}"
        status=$?

    fi
    
    return $status
}

