#!/usr/bin/env osascript

on run argv
	tell application "Xcode"
		load documentation set with path item 1 of argv
	end tell
end run
