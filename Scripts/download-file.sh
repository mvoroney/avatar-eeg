#!/bin/sh

# load settings.
source "${INPUT_FILE_PATH}"

if [ "${DOWNLOAD_PATH}" = "" ]; then

    DOWNLOAD_PATH="${HOME}/Downloads/${NAME}-${VERSION}.${EXTENSION}"

fi

INFO_FILE="${DERIVED_FILES_DIR}/${INPUT_FILE_BASE}.${EXTENSION}-local"

RESPONSE=200
RESULT=0

if [ ! -e "${DOWNLOAD_PATH}" ]; then

    echo "Downloading: ${FILE_URL} to ${DOWNLOAD_PATH}"
    RESPONSE=$(curl -w "%{http_code}" -L -s -S -o "${DOWNLOAD_PATH}" "${FILE_URL}")
    RESULT=$?

fi

if [ ${RESPONSE} -ge 400 ]; then

    echo "HTTP Error: ${RESPONSE}"
    RESULT=${RESPONSE}

else

    echo "Download successfull."

fi

cat > ${INFO_FILE} <<INFO_FILE_EOF

NAME=${NAME}
VERSION=${VERSION}
EXTENSION=${EXTENSION}
REL_PATH=${REL_PATH}
INSTALL_PATH=${INSTALL_PATH}
FILE_PATH=${DOWNLOAD_PATH}

INFO_FILE_EOF

exit ${RESULT}
