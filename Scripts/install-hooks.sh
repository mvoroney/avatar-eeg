#!/usr/bin/env bash

basedir=$(dirname $(dirname `readlink $0 || ls $PWD/$0 2> /dev/null || ls $0 2> /dev/null`))

#
# Creates links to all files in Scripts/Hooks, in .git/hooks, that don't
# already exist in .git/hooks. Files in the Scripts/Hooks directory are 
# expected to include the .sh suffix, since this is the only way to get
# syntax highlighting in xcode...
#
for i in `ls ${basedir}/Scripts/Hooks | sed 's/.sh$//'`; do
    if [ ! -e "${basedir}/.git/hooks/$i" ]; then
        ln -s "${basedir}/Scripts/Hooks/$i.sh" "${basedir}/.git/hooks/$i"
        echo installed Scripts/Hooks/$i in .git/hooks/$i
    fi
done
