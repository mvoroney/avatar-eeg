#!/bin/sh

# load settings
source "${INPUT_FILE_PATH}"

MOUNTPOINT="/Volumes/${NAME}-${VERSION}"
INFO_FILE="${DERIVED_FILES_DIR}/${INPUT_FILE_BASE}.${EXTENSION}-mount"

if [ ! -d ${MOUNTPOINT} ] || [ $(ls ${MOUNTPOINT} | wc -w) -eq 0 ]; then

    echo "Mounting ${FILE_PATH} at ${MOUNTPOINT}"
    mkdir -p "${MOUNTPOINT}"
    hdiutil attach "${FILE_PATH}" -mountpoint "${MOUNTPOINT}"
    RESULT=$?

fi

cat > ${INFO_FILE} <<INFO_FILE_EOF

NAME=${NAME}
VERSION=${VERSION}
REL_PATH=${REL_PATH}
INSTALL_PATH=${INSTALL_PATH}
MOUNT_PATH=${MOUNTPOINT}

INFO_FILE_EOF

exit ${RESULT}
