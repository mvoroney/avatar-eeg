#!/usr/bin/env bash

base_dir=$(dirname $(dirname $(dirname `readlink $0 || ls $PWD/$0 2> /dev/null || ls $0 2> /dev/null`)))
source "$base_dir/Scripts/utils.sh"

# ensures that ocunit testing has been run.
function ensure_check {

    echo "[POLICY] ensure testing is green..."
    
    # TODO: is there seriously no way to just check the status?
    xcodebuild -target test | grep 'failed\|error' > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "[POLICY] You have failing tests!"
        echo "[POLICY] Always run ocunit testing through xcode before committing."
        echo "[POLICY] To skip this check, use '--no-verify' (not recommended)."
        exit 1
    fi
}

# ensure that changed files are properly licenced.
function ensure_license {

    echo "[POLICY] ensure changed/new files are licenced properly..."

    local preamble="$base_dir/licence.preamble"

    # get the list of files staged to be commited.
    # TODO: is there really no better way to do this?
    local changed=`git status -s | grep '^[A-Z] ' | sed 's/^[A-Z] *//; s/^.* -> //'`

    # limit check to source files.
    changed=`echo $changed | grep '\.\(c\|m\|h\)$'`
    
    echo "changed files: $changed"
    
    for file in $changed; do
       
        contains-block $file $preamble
        if [ $? -ne 0 ]; then
            echo "[POLICY] Changed file '$file' does not include the licence"
            echo "[POLICY] preamble. Please insert the contents of '$preamble'"
            echo "[POLICY] verbatim at the top of the file in a block comment."
            echo "[POLICY] You can use Scripts/licence-files.sh to do this for you."
            exit 1
        fi
    done
}

ensure_check
ensure_license
echo "[POLICY] everyhing looks good."

exit 0
