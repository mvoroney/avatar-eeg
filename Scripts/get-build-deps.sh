#!/usr/bin/env bash

source "${SRCROOT}/Scripts/utils.sh"

STATUS=0

# note: the following files are assueme to be dmg packages.

# download doxygen documentation generator
#
DOXYGEN_URL="http://ftp.stack.nl/pub/users/dimitri"
DOXYGEN_VERSION="1.8.4"
DOXYGEN_NAME="Doxygen"

# TODO: code at this level should not be version dependent
# NOTE: ocmock v1.77 needed for xcode 3.2 support.
#
OCMOCK_URL="http://ocmock.org/downloads"
OCMOCK_VERSION="2.1.1"
OCMOCK_NAME="ocmock"


for dep in OCMOCK DOXYGEN; do
    install_dmg ${CACHE_ROOT} ${SRCROOT} $(eval "echo \${${dep}_NAME}") $(eval "echo \${${dep}_VERSION}") $(eval "echo \${${dep}_URL}")
    STATUS=$?
done

# download xunit style XML report generator for OCUnit
#
BPOCUXR_URL="https://raw.github.com/threeve/BPOCUnitXMLReporter/master"
BPOCUXR_FILE="BPOCUnitXMLReporter.m"
if [ $STATUS -a ! -f "${SRCROOT}/3rdParty/${BPOCUXR_FILE}" ]; then

    echo "BPOCUnitXMLReporter.m is missing. downloading from ${BPOCUXR_URL}..."
    mkdir -p "${SRCROOT}/3rdParty"
    curl -L -o "${SRCROOT}/3rdParty/${BPOCUXR_FILE}" "${BPOCUXR_URL}/${BPOCUXR_FILE}"
    STATUS=$?
fi


exit $STATUS


