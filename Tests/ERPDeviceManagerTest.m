/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSData.h>
#import <Foundation/NSValue.h>
#import <IOBluetooth/objc/IOBluetoothDevice.h>
#import <IOBluetooth/IOBluetoothUtilities.h>
#import <OCMock/OCMock.h>
#import "ERPDeviceManagerTest.h"
#import "ERPDeviceManager.h"

#import <stdlib.h>

/// \todo is there a cleaner way to do this? use method swizling? forwarding? dynamic resolution?
/// only needed to allow class methods to forward to an OCMock instance.
@interface IOBluetoothDeviceMockAdapter : IOBluetoothDevice

-(id) deviceWithAddress: (const BluetoothDeviceAddress*) address;
+(id) deviceWithAddress: (const BluetoothDeviceAddress*) address;
 
@end

static id g_deviceInstance = nil;

@implementation IOBluetoothDeviceMockAdapter

+(id) deviceWithAddress:(const BluetoothDeviceAddress*) address {
    
    if (nil != g_deviceInstance) {
        // invoke mock method on mock instance of this class.
        return [g_deviceInstance deviceWithAddress: address];
    } else {
        // invoke the original factory method. why doesn't super work here?
        return [IOBluetoothDevice deviceWithAddress: address];
    }
}

// required for OCMock to tie into.
-(id) deviceWithAddress:(const BluetoothDeviceAddress*) address {
    (void)address;
    return nil;
}

@end

static BluetoothDeviceAddress address = {{0x00, 0x16, 0xa4, 0x03, 0x22, 0x7c}};
typedef void(^deviceMockBlock)(id, NSValue*, int);

@implementation ERPDeviceManagerTest

// adds memory to autorelease pool.
/// \todo this should probably be added to a production class. super handy.
-(void*) allocate:(size_t) block_size {
    void * memory = malloc(block_size);
    if (NULL == memory) {
        [NSException raise: @"OutOfMemoryException" format: @"Could not allocate block of size: %zd", block_size];
    }
    // add memory to autorelease pool
    [NSData dataWithBytesNoCopy: memory length: block_size];
    
    return memory;
}

// will autorelease memory.
-(BluetoothDeviceAddress*) randomDeviceAddress {
    BluetoothDeviceAddress* result = (BluetoothDeviceAddress*)[self allocate: sizeof(BluetoothDeviceAddress)];
    
    u_int32_t* start = (u_int32_t*)result;
    // int division should also prevent buffer overfow.
    u_int32_t* end = (u_int32_t*)start + (sizeof(BluetoothDeviceAddress) / sizeof(u_int32_t));
    
    // randomize memory
    while (start < end) *start++ = arc4random();
    
    return result;
}

- (void) deviceManagerWithMocks: (id*) mockDevices withCount: (NSUInteger) nDevices withBlock: (deviceMockBlock) initBlock  {
    id mockDataDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    g_deviceInstance = [OCMockObject mockForClass: [IOBluetoothDeviceMockAdapter class]];
    
    // setup mocks.
    for (unsigned int i = 0; i < nDevices; i++) {
        BluetoothDeviceAddress* addr = [self randomDeviceAddress];
        NSValue* vaddr = [NSValue valueWithBytes: &addr objCType: @encode(const BluetoothDeviceAddress*)];
        
        mockDevices[i] = [OCMockObject mockForClass: [IOBluetoothDevice class]];
        initBlock(mockDevices[i], vaddr, i);
        
        [[[g_deviceInstance expect] andReturn: mockDevices[i]] deviceWithAddress: addr];
        [deviceManager addDevice: addr withDelegate: mockDataDelegate];
    }
}

-(void) setUp {
    deviceManager = [[ERPDeviceManager alloc] initWithDeviceFactory: [IOBluetoothDeviceMockAdapter class]];
}

-(void) tearDown {
    g_deviceInstance = nil;
    if (nil != deviceManager) {
        [deviceManager release];
    }
}

-(void) test_deviceFactory_createsCorrectKind {
    g_deviceInstance = [OCMockObject mockForClass: [IOBluetoothDeviceMockAdapter class]];
    [[[g_deviceInstance expect] andReturn: [IOBluetoothDevice deviceWithAddress: &address]] deviceWithAddress: &address];
    id mockDataDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    
    id result = [deviceManager addDevice: &address withDelegate: mockDataDelegate];
    
    [testDevice verify];
    STAssertNotNil(result, @"Did not instantiate device for address: %@.", IOBluetoothNSStringFromDeviceAddress(&address));
    STAssertEqualObjects(IOBluetoothNSStringFromDeviceAddress(&address), [result getAddressString], @"wrong device?");
    STAssertTrue([result isMemberOfClass: [IOBluetoothDevice class]], @"Device has wrong class? got: %@", [result class]);
}

-(void) test_addDevice_addsAndInitializesDevice {
    id mockDataDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    
    id dev = [deviceManager addDevice: &address withDelegate: mockDataDelegate];
    
    NSArray * devices = [deviceManager queryDevices];
    STAssertTrue(1 == [deviceManager count], @"should only find the devices we added. got: %@", devices);
    STAssertEqualObjects(dev, [devices objectAtIndex: 0], @"should be the same object.");
}

/// \todo memory error in this test? how is this possible?
-(void) test_addDevice_expandsBeyondInitialSize {
    id mockDataDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    
    // should force the internal array to grow
    NSUInteger nDevices = DEFAULT_DEVICE_LIST_SIZE + 50 * DEFAULT_DEVICE_LIST_INCREMENT + 2;
    
    for (unsigned int i = 0; i < nDevices; i++) {
        [deviceManager addDevice: [self randomDeviceAddress] withDelegate: mockDataDelegate];
    }
    
    STAssertTrue(nDevices == [deviceManager count], @"should find all the devices we added. got: %@", [deviceManager queryDevices]);
}

-(void) test_removeDevice_stopsDeviceAndRemoves {
    NSUInteger nDevices = DEFAULT_DEVICE_LIST_SIZE;
    
    id* mockDevices = (id*)[self allocate: sizeof(id) * nDevices];
    __block BluetoothDeviceAddress** addrs = (BluetoothDeviceAddress**)[self allocate: nDevices * sizeof(BluetoothDeviceAddress*)];
    
    // setup mocks.
    [self deviceManagerWithMocks: mockDevices withCount: nDevices withBlock: ^(id mock, NSValue* addr, int i) {
        // save for validation.
        [addr getValue: &addrs[i]];
        
        [[[mock expect] andReturnValue: addr] getAddress];
        
        BOOL bTemp = (i % 2 ? YES : NO);
        [[[mock expect] andReturnValue: OCMOCK_VALUE(bTemp)] isConnected];
        
        if (i % 2) {
            [[[mock expect] andReturnValue: OCMOCK_VALUE((IOReturn){kIOReturnSuccess})] closeConnection];
        }
        
        // allow multiple subsequent calls.
        [[[mock stub] andReturnValue: addr] getAddress];
    }];
    
    STAssertTrue(nDevices == [deviceManager count], @"wrong number of devices. expected: %d", nDevices);
    
    for (unsigned int i = 0; i < nDevices; i++) {
        ERPDevice* dev = [deviceManager removeDevice: addrs[i]];
        const BluetoothDeviceAddress * gotAddr = [dev getAddress];

        STAssertNotNil(dev, @"could not remove device: %@", IOBluetoothNSStringFromDeviceAddress(addrs[i]));
        STAssertTrue(NULL != gotAddr, @"removed device has no address?");
        STAssertEqualObjects(IOBluetoothNSStringFromDeviceAddress(addrs[i]), IOBluetoothNSStringFromDeviceAddress(gotAddr), @"removed wrong device.");
        
        /// \todo change to use [deviceManager queryDevices: withObject:] once implemented.
        /// \todo use predicate function once implemented.
        STAssertTrue(NSNotFound == [[deviceManager queryDevices] indexOfObject: dev], @"device was not removed: %@", IOBluetoothNSStringFromDeviceAddress(addrs[i]));
        STAssertNil(dev = [deviceManager removeDevice: addrs[i]], @"device was not properly removed the first time: %@", IOBluetoothNSStringFromDeviceAddress(addrs[i]));
    }
        
    STAssertTrue(0 == [deviceManager count], @"expected no devices, got: %d", [deviceManager count]);
    
    for (unsigned int i = 0; i < nDevices; i++) {
        [mockDevices[i] verify];
    }
}

-(void) test_startDevices_startsAllManagedDevices {
    NSUInteger nDevices = DEFAULT_DEVICE_LIST_SIZE;
    
    /// \todo can't use stack allocated array? incompatible pointer cast warning.
    id* mockDevices = (id*)[self allocate: sizeof(id) * nDevices];
    
    // setup mocks.
    [self deviceManagerWithMocks: mockDevices withCount: nDevices withBlock: ^(id mockDevice, NSValue* addr, int i) {
        (void)addr;
        (void)i;
        
        [[[[mockDevice expect] andDo: ^(NSInvocation * arg1) {
            (void)arg1;
            [deviceManager connectionComplete: mockDevice status: kIOReturnSuccess];
        }] andReturnValue: OCMOCK_VALUE((IOReturn){kIOReturnSuccess})] openConnection: deviceManager];
        
        [[[mockDevice stub] andReturnValue: OCMOCK_VALUE((BOOL){NO})] isConnected];
    }];
    
    /// \todo WTF? why is this extra retain needed? crashes on teardown otherwise.
    [deviceManager retain];
    [deviceManager startDevices];
    
    for (unsigned int i = 0; i < nDevices; i++) {
        [mockDevices[i] verify];
    };
}

-(void) test_count_reportsNumberOfAddedDevices {
    id mockDataDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    NSUInteger nDevices = DEFAULT_DEVICE_LIST_SIZE;
    NSUInteger expectedCount = 0;
    NSUInteger gotCount = 0;
    
    for (expectedCount = 0; expectedCount < nDevices; expectedCount++) {
        gotCount = [deviceManager count];
        STAssertTrue(expectedCount == gotCount, @"wrong count: got: %lu, expected: %lu.", gotCount, expectedCount);
        [deviceManager addDevice: [self randomDeviceAddress] withDelegate: mockDataDelegate];
    }
    
    gotCount = [deviceManager count];
    STAssertTrue(expectedCount == gotCount, @"wrong count: got: %lu, expected: %lu.", gotCount, expectedCount);
}

-(void) test_queryDevices_noarg_returnsAllManagedDevices {
    id mockDataDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    NSArray * devices;
    
    devices = [deviceManager queryDevices];
    STAssertTrue(0 == [devices count], @"should be no devices.");
    
    id dev = [deviceManager addDevice: &address withDelegate: mockDataDelegate];
    
    devices = [deviceManager queryDevices];
    STAssertTrue(1 == [devices count], @"should only find the devices we added. got: %@", devices);
    STAssertEqualObjects(dev, [devices objectAtIndex: 0], @"should be the same object.");    
}

-(void) test_queryDevices_worksWithBoolSelector {
    NSUInteger nDevices = DEFAULT_DEVICE_LIST_SIZE;
    
    /// \todo can't use stack allocated array? incompatible pointer cast warning.
    id* mockDevices = (id*)[self allocate: sizeof(id) * nDevices];
    
    __block NSUInteger nConnected;

    // setup mocks.
    [self deviceManagerWithMocks: mockDevices withCount: nDevices withBlock: ^(id mockDevice, NSValue* addr, int i) {
        (void)addr;
        
        BOOL bTemp = (i % 2 ? YES : NO);
        [[[mockDevice expect] andReturnValue: OCMOCK_VALUE(bTemp)] isConnected];
        [[[mockDevice stub] andReturnValue: OCMOCK_VALUE(bTemp)] isConnected];
        
        if (i % 2) {
            [[[mockDevice stub] andReturnValue: OCMOCK_VALUE((IOReturn){kIOReturnSuccess})] closeConnection];
            nConnected++;
        }
    }];

    NSArray * devices;
    devices = [deviceManager queryDevices: @selector(isConnected)];
    
    for (unsigned int i = 0; i < nDevices; i++) {
        [mockDevices[i] verify];
    };
    
    STAssertTrue(nConnected == [devices count], @"should find all the devices we added. got: %@", devices);    
}

-(void) test_queryDevices_worksWithObjectSelector {
    NSUInteger nDevices = DEFAULT_DEVICE_LIST_SIZE;
    
    /// \todo can't use stack allocated array? incompatible pointer cast warning.
    id* mockDevices = (id*)[self allocate: sizeof(id) * nDevices];
    __block id* addrs = (id*)[self allocate: sizeof(id) * nDevices];
    
    // setup mocks.
    [self deviceManagerWithMocks: mockDevices withCount: nDevices withBlock: ^(id mockDevice, NSValue* addr, int i) {
        NSString * name = [addr description];
        [[[mockDevice expect] andReturn: name] getName];
        addrs[i] = name;
        
        [[[mockDevice stub] andReturnValue: OCMOCK_VALUE((BOOL){NO})] isConnected];
        [[[mockDevice stub] andReturn: name] getName];
    }];
    
    for (unsigned int i = 0; i < nDevices; i++) {
        NSArray * devices;
        NSString * expectedName = addrs[i];
        devices = [deviceManager queryDevices: @selector(getName) equalTo: expectedName];
        
        STAssertTrue(1 == [devices count], @"expected 1 device, got: %lu", [devices count]);
        
        NSString * gotName = [[devices objectAtIndex: 0] getName];
        
        STAssertTrue([expectedName isEqual: gotName], @"attribute for matching objects differ: Expected: %@, got: %@.", expectedName, gotName);
    }
    
    for (unsigned int i = 0; i < nDevices; i++) {
        [mockDevices[i] verify];
    }
}

-(void) test_queryDevices_worksWithValueSelector {
    NSUInteger nDevices = DEFAULT_DEVICE_LIST_SIZE;
    
    /// \todo can't use stack allocated array? incompatible pointer cast warning.
    id* mockDevices = (id*)[self allocate: sizeof(id) * nDevices];
    __block id* addrs = (id*)[self allocate: sizeof(id) * nDevices];
    
    // setup mocks.
    [self deviceManagerWithMocks: mockDevices withCount: nDevices withBlock: ^(id mockDevice, NSValue* addr, int i) {
        [[[mockDevice expect] andReturnValue: addr] getAddress];
        addrs[i] = addr;
        
        [[[mockDevice stub] andReturnValue: OCMOCK_VALUE((BOOL){NO})] isConnected];
        [[[mockDevice stub] andReturnValue: addr] getAddress];
    }];
    
    for (unsigned int i = 0; i < nDevices; i++) {
        NSArray * devices;
        NSValue * expectedAddr = addrs[i];
        const BluetoothDeviceAddress * rawAttr;
        devices = [deviceManager queryDevices: @selector(getAddress) equalTo: expectedAddr];
        
        STAssertTrue(1 == [devices count], @"expected 1 device, got: %lu", [devices count]);

        rawAttr = [[devices objectAtIndex: 0] getAddress];
        NSValue * gotAttr = [NSValue valueWithBytes: &rawAttr objCType: [expectedAddr objCType]];
        
        STAssertTrue([expectedAddr isEqualToValue: gotAttr], @"attribute for matching objects differ: Expected: %@, got: %@.", expectedAddr, gotAttr);
    }
    
    for (unsigned int i = 0; i < nDevices; i++) {
        [mockDevices[i] verify];
    };
}


@end
