/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import "AEDeviceTest.h"
#import "AEDevice.h"

// NB: this is the address of the Avatar EEG device. change as needed.
static const BluetoothDeviceAddress devAddr = {0x00, 0x16, 0xa4, 0x03, 0x22, 0x7c};

@implementation AEDeviceTest

-(void)testConnectionShouldOpenOnConnect {
    AEDevice * dev = [[AEDevice alloc] init];
    
    [dev connect:&devAddr];
    
    STAssertTrue([dev isConnected], @"device should be connected."); 
}

-(void)testConnectionShouldOpenOnInitWithAddress {
    
}

-(void)testConnectionShouldDisconnectOnDestruction {
    
}


@end
