/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <SenTestingKit/SenTestingKit.h>
#import "ERPChannelSetupHandler.h"

@interface ERPChannelSetupHandlerTest : SenTestCase

-(void) test_connectionTransition_openBasebandConnection_to_performSDPQuery;

@end
