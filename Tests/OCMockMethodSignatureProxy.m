/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSEXception.h>
#import "OCMockMethodSignatureProxy.h"

#import <string.h>

static BOOL OCMockMethodSignatureProxy_debug = YES;

/// \todo extract this to a debug utils header?
#define DEBUG_LOG(format, ...) \
    if (OCMockMethodSignatureProxy_debug) \
    NSLog(@"%d: %s: %s: %@", __LINE__, __FILE__, __func__, [NSString stringWithFormat: format, __VA_ARGS__])

@implementation OCMockMethodSignatureProxy

+(Class) getClass {
    [NSException 
     raise: @"OperationNotImplementedException" 
     format: @"Subclasses of OCMockMethodSignatureProxy must implement +(Class) getClass."];
    
    return Nil;
}

+(BOOL) instancesRespondToSelector:(SEL) aSelector {
    DEBUG_LOG(@"%@.", NSStringFromSelector(aSelector));
    
    return [[self getClass] instancesRespondToSelector: aSelector];
}

/// \todo we should probably try to generalize this process a bit more...
+(NSMethodSignature *) instanceMethodSignatureForSelector:(SEL) selector {
    DEBUG_LOG(@"%@.", NSStringFromSelector(selector));
    
    NSMethodSignature * sig = [[self getClass] instanceMethodSignatureForSelector: selector];
    // try to guess some appropriate size for our buffer.
    NSMutableString * rawSig = [NSMutableString stringWithCapacity: [sig numberOfArguments] * 8];
    
    // setup return type.
    [rawSig appendString: [NSString stringWithCString: [sig methodReturnType] encoding: NSASCIIStringEncoding]];
    
    // first 2 arguments.
    for (NSUInteger i = 0; i < 2; i++) {
        [rawSig appendString: [NSString 
                               stringWithCString: [sig getArgumentTypeAtIndex: i] 
                               encoding: NSASCIIStringEncoding]];
    }
    
    // transform user argument types.
    for (NSUInteger i = 2; i < [sig numberOfArguments]; i++) {
        // replace character string encodings with pointer to unsigned character encodings
        [rawSig appendString: [[NSString stringWithCString: [sig getArgumentTypeAtIndex: i]
                                                  encoding: NSASCIIStringEncoding] 
                               stringByReplacingOccurrencesOfString: @"*" withString: @"^C"]];
    }
    
    NSMethodSignature * newSig = [NSMethodSignature signatureWithObjCTypes: [rawSig UTF8String]];
    return newSig;
}

-(NSMethodSignature *) methodSignatureForSelector:(SEL) selector {
    DEBUG_LOG(@"%@.", NSStringFromSelector(selector));
    
    return [[self class] instanceMethodSignatureForSelector: selector];
}

-(BOOL) respondsToSelector:(SEL) aSelector {
    DEBUG_LOG(@"%@.", NSStringFromSelector(aSelector));
    
    return ([[self class] instancesRespondToSelector: aSelector]);
}

@end
