/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSProxy.h>


/// technique taken from: http://stackoverflow.com/a/14816966
@interface OCMockMethodSignatureProxy : NSProxy

+(Class) getClass;
+(BOOL) instancesRespondToSelector:(SEL) aSelector;
+(NSMethodSignature *) instanceMethodSignatureForSelector:(SEL)selector;
-(NSMethodSignature *) methodSignatureForSelector:(SEL)selector;

-(BOOL) respondsToSelector:(SEL) aSelector;

@end

/// \todo move this to a utility macro header.
#define PASTE(x, y) PASTE_(x, y)
#define PASTE_(x, y) x ## y

#define MAKE_PROXY(class_name, extension)                                       \
@interface PASTE(class_name, extension) : OCMockMethodSignatureProxy            \
@end                                                                            \
static Class PASTE(PASTE(class_name, extension), _Class) = Nil;                 \
@implementation PASTE(class_name, extension)                                    \
+(void) initialize {                                                            \
    PASTE(PASTE(class_name, extension), _Class) = [class_name class];           \
}                                                                               \
+(Class) getClass {                                                                \
    return PASTE(PASTE(class_name, extension), _Class);                         \
}                                                                               \
@end

