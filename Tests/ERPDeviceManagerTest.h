/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <SenTestingKit/SenTestingKit.h>

@class ERPDeviceManager;

@interface ERPDeviceManagerTest : SenTestCase {
    id testDevice;
    ERPDeviceManager * deviceManager;
}

-(void) setUp;
-(void) tearDown;

-(void) test_deviceFactory_createsCorrectKind;
-(void) test_addDevice_addsAndInitializesDevice;
-(void) test_addDevice_expandsBeyondInitialSize;
-(void) test_removeDevice_stopsDeviceAndRemoves;

-(void) test_startDevices_startsAllManagedDevices;

-(void) test_count_reportsNumberOfAddedDevices;
-(void) test_queryDevices_noarg_returnsAllManagedDevices;
-(void) test_queryDevices_worksWithBoolSelector;
-(void) test_queryDevices_worksWithObjectSelector;

@end

