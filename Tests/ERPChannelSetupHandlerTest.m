/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <IOBluetooth/objc/IOBluetoothDevice.h>
#import <IOBLuetooth/objc/IOBluetoothRFCOMMChannel.h>
#import <IOBluetooth/objc/IOBluetoothSDPServiceRecord.h>
#import <IOKit/IOReturn.h>
#import <Foundation/NSInvocation.h>

#import <OCMock/OCMock.h>
#import "ERPChannelSetupHandlerTest.h"
#import "ERPDataPushListener.h"
#import "ERPDevicePushListener.h"

#import "OCMockMethodSignatureProxy.h"

MAKE_PROXY(IOBluetoothSDPServiceRecord, Proxy)
MAKE_PROXY(IOBluetoothDevice, Proxy)
MAKE_PROXY(IOBluetoothRFCOMMChannel, Proxy)


@implementation ERPChannelSetupHandlerTest

-(void) test_connectionTransition_openBasebandConnection_to_performSDPQuery {
    id mockDevice = [OCMockObject mockForClass: [IOBluetoothDeviceProxy class]];
    id mockDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    id mockNotify = [OCMockObject mockForProtocol: @protocol(IOBluetoothRFCOMMChannelDelegate)];
    
    ERPChannelSetupHandler * handler = 
        [[ERPChannelSetupHandler alloc] 
         initWithDevice: mockDevice andDelegate: mockDelegate andNotify: mockNotify];

    [[mockDevice expect] performSDPQuery: handler];
    
    // start the call sequence
    [handler connectionComplete: mockDevice status: kIOReturnSuccess]; 
    [handler release];
    
    [mockDevice verify];
    [mockDelegate verify];
    [mockNotify verify];
}

-(void) test_connectionTransition_peformSDPQuery_to_openRFCOMMChannel {
    __block id mockDevice = [OCMockObject mockForClass: [IOBluetoothDeviceProxy class]];
    __block id mockChannel = [OCMockObject mockForClass: [IOBluetoothRFCOMMChannelProxy class]];
    id mockServiceRecord = [OCMockObject mockForClass: [IOBluetoothSDPServiceRecordProxy class]];
    id mockDelegate = [OCMockObject mockForProtocol: @protocol(ERPDataPushListener)];
    id mockNotify = [OCMockObject mockForProtocol: @protocol(IOBluetoothRFCOMMChannelDelegate)];
    
    BluetoothRFCOMMChannelID chanID = 1;
    ERPChannelSetupHandler * handler = 
        [[ERPChannelSetupHandler alloc] 
         initWithDevice: mockDevice andDelegate: mockDelegate andNotify: mockNotify];
    
    
    [[[mockDevice expect] andDo: ^(NSInvocation* arg1) {
        ERPChannelSetupHandler* myHandler = nil;
        
        [arg1 getArgument: &myHandler atIndex: 2];
        STAssertTrue(myHandler == handler, @"Test setup error: expected: %@ got: %@ ", handler, myHandler);
        
        // some problem on this line.
        [myHandler sdpQueryComplete: mockDevice status: kIOReturnSuccess];
    }] performSDPQuery: handler];
    
    
    [[[mockDevice expect] andReturn: [NSArray arrayWithObjects: mockServiceRecord, nil]] servicesBridge];
    [[[mockServiceRecord expect] andDo: ^(NSInvocation* arg1) {
    
    //andReturnValue: OCMOCK_VALUE((IOReturn){kIOReturnSuccess})
        BluetoothRFCOMMChannelID * cID;
        
        NSLog(@"%d: getRFCOMMChannelID expectation.", __LINE__);
        [arg1 getArgument: &cID atIndex: 2];
        NSLog(@"%d: getRFCOMMChannelID expectation.", __LINE__);
        //STAssertTrue(&chanID == cID, @"Test setup error: expected: %@ got: %@", &chanID, cID);
        NSLog(@"%d: getRFCOMMChannelID expectation.", __LINE__);
        *cID = chanID;
        STAssertTrue(*cID == 1, @"Test setup error: expected 1 got: %d.", *cID);
        NSLog(@"%d: getRFCOMMChannelID expectation.", __LINE__);
        
        IOReturn v = kIOReturnSuccess;
        [arg1 setReturnValue: &v];
        
        NSLog(@"%d: getRFCOMMChannelID expectation.", __LINE__);
    }] getRFCOMMChannelID: [OCMArg anyPointer]];
    
    
    [[[[mockDevice expect] andDo:^(NSInvocation* arg1) {
        ERPDevicePushListener* rfcommchannelDelegate = nil;
        
        [arg1 getArgument: &rfcommchannelDelegate atIndex: 4];
        
        [rfcommchannelDelegate rfcommChannelOpenComplete: mockChannel status: kIOReturnSuccess];
        
    }] andReturnValue: OCMOCK_VALUE((IOReturn){kIOReturnSuccess})] 
     openRFCOMMChannelAsync: [OCMArg anyPointer] withChannelID: chanID delegate: [OCMArg isNotNil]];
    
    // start the call sequence
    [mockDevice performSDPQuery: handler]; 
    [handler release];
    
    [mockDevice verify];
    [mockChannel verify];
    [mockServiceRecord verify];
    [mockDelegate verify];
}


@end
