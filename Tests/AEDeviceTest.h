/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <SenTestingKit/SenTestingKit.h>

@interface AEDeviceTest : SenTestCase {

}

-(void)testConnectionShouldOpenOnConnect;
-(void)testConnectionShouldOpenOnInitWithAddress;
-(void)testConnectionShouldDisconnectOnDestruction;

@end
