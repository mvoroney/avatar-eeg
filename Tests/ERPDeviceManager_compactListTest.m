/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import "ERPDeviceManager_compactListTest.h"
#import <stdlib.h>
#import <string.h>

// private utility of ERPDeviceManager
ERPDeviceList firstEmptySlot(id** listRef, NSUInteger * listSize, NSUInteger increment);
NSUInteger compactList(id* list, NSUInteger size);

@implementation ERPDeviceManager_compactListTest

-(ERPDeviceList) zeroList:(NSUInteger) size {
    ERPDeviceList temp = (ERPDeviceList)calloc(sizeof(ERPDevice*), size);
    // add memory to autorelease pool
    [NSData dataWithBytes: temp length: sizeof(ERPDevice*) * listCapacity];
    
    return temp;
}

-(NSInteger) checksumList:(ERPDeviceList) list length:(NSUInteger) len {
    ERPDeviceList start = list;
    ERPDeviceList end = start + len;
    NSInteger accumulator = 0;
    
    while (start < end) {
        accumulator += (NSUInteger)*start;
        start++;
    }
    
    return accumulator;
}

-(void) setUp {
    listCapacity = 10;
    devices = [self zeroList: listCapacity];
}

-(void) tearDown {
    // list autoreleased
    listCapacity = 0;
}

-(void) test_compactList_compactsEmptyList {
    NSUInteger size = compactList(devices, listCapacity);
    NSData * dtemp = [NSData dataWithBytes: devices length: listCapacity * sizeof(ERPDevice*)];
    
    STAssertTrue(0 == size, @"size: expected: 0, got: %lu", size);
    STAssertTrue(0 == memcmp([self zeroList: listCapacity], devices, sizeof(ERPDevice*) * listCapacity), @"list should be zeroed: got: %@", dtemp);
}

-(void) test_compactList_compactsSparseList {
    ERPDeviceList expectedList = [self zeroList: listCapacity];
    NSUInteger expectedSize = 0;
    for (NSUInteger i = 0; i < listCapacity; i++) {
        // try 25% density.
        if (0 == (arc4random() % 4)) {
            devices[i] = (ERPDevice*)(i + 1);
            expectedList[expectedSize] = devices[i];
            
            expectedSize++;
        }
    }
    
    NSUInteger listBytes = listCapacity * sizeof(ERPDevice*);
    NSUInteger size = compactList(devices, listCapacity);
    NSData * dgot = [NSData dataWithBytes: devices length: listBytes];
    NSData * dexpected = [NSData dataWithBytes: expectedList length: listBytes];
    
    STAssertTrue(size == expectedSize, @"wrong size: expected: %lu for %@, got %lu for %@", expectedSize, dexpected, size, dgot);
    STAssertTrue([self checksumList: expectedList length: listCapacity] == [self checksumList: devices length: listCapacity], @"lists differ expected: %@ got: %@", dexpected, dgot);
    
}

-(void) test_compactList_preservesDenseList {
    ERPDeviceList expectedList = [self zeroList: listCapacity];
    NSUInteger expectedSize = listCapacity / 2;
    for (NSUInteger i = 0; i < expectedSize; i++) {
        devices[i] = (ERPDevice*)(i + 1);
        expectedList[i] = devices[i];
    }
    
    NSUInteger listBytes = listCapacity * sizeof(ERPDevice*);
    NSUInteger size = compactList(devices, listCapacity);
    NSData * dgot = [NSData dataWithBytes: devices length: listBytes];
    NSData * dexpected = [NSData dataWithBytes: expectedList length: listBytes];
    
    STAssertTrue(expectedSize == size, @"size: expected: %lu for %@, got: %lu for %@", expectedSize, dexpected, size, dgot);
    STAssertTrue(0 == memcmp(expectedList, devices, listBytes), @"lists differ expected: %@ got: %@", dexpected, dgot);
}

-(void) test_compactList_worksWithRepeatedCalls {
    
}


@end
