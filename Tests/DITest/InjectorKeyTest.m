/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
//
//  InjectorKeyTest.m
//  plist-test
//
//  Created by Testing on 13-03-22.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "InjectorKeyTest.h"
#import "InjectorKey.h"

@implementation InjectorKeyTest

-(void) setUp {
    key1 = [[InjectorKey alloc] init: [InjectorKey class] inContext: [InjectorKeyTest class]];
    key2 = [InjectorKey keyWith: [InjectorKey class] inContext: [InjectorKeyTest class]];
    key3 = [InjectorKey keyWith: [InjectorKey class] inContext: nil];
}

-(void) tearDown {
    // keys autoreleased.
}

-(void) test_initWith_Creates_ValidKey {
    STAssertNotNil(key1, @"key should be initialized");
    STAssertTrue([key1 isKindOfClass: [InjectorKey class]], 
                  @"key should be InjectorKey impl: found %@ instead.", [key1 class]);
}

-(void) test_keyWith_Creates_ValidKey {
    STAssertNotNil(key2, @"key should be initialized");
    STAssertTrue([key2 isKindOfClass: [InjectorKey class]], 
                  @"key should be InjectorKey impl: found %@ instead.", [key1 class]);
}

-(void) test_isEqual_YES_For_EqualKeys {
    STAssertTrue([key1 isEqual: key2], @"keys should be equal: key1=%@ key2=%@", key1, key2);
}

-(void) test_isEqual_NO_For_InEqualKeys {
    STAssertFalse([key1 isEqual: key3], @"keys should NOT be equal: key1=%@ key2=%@", key1, key3);
}

-(void) test_hash_Handles_NilContext {
    STAssertNotNil(key3, @"key should be initialized");
    // \todo should probably verify hash key or something...
    STAssertEquals(
                   (NSUInteger)[key3 hash], 
                   (NSUInteger)[key3 interface] * INTERFACE_PRIME,
                   @"has for nil context should be the same as hash of the interface: expected.");
    STAssertTrue(0 != [key3 hash], @"hash should not be zero for non-zero input: %x.", [key3 hash]);
}

-(void) test_hash_Uniqueness {
    /// \todo is there any clean way to do this?
}

-(void) test_copyWithZone_Copies {
    InjectorKey * tkey = [key1 copyWithZone: nil];
    
    STAssertNotNil(tkey, @"copied key should not be nil.");
    STAssertTrue([tkey isKindOfClass: [InjectorKey class]], 
                 @"key should be InjectorKey impl: found %@ instead.", [tkey class]);
    
    // immutable classes implement copy with retain.
    //STAssertTrue(tkey != key1, @"copied key should have different address: tkey=%x key1=%x.", tkey, key1);
    STAssertTrue([key1 isEqual: key2], @"keys should be equal: tkey=%@ key1=%@", tkey, key1);
    
    // copy should survive release of copied... how to test?
    [tkey release];
}

@end
