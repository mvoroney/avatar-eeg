/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
//
//  RuntimeUtilsTest.h
//  avatar-eeg-framework
//
//  Created by Testing on 13-03-31.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>


@interface RuntimeUtilsTest : SenTestCase {
    SEL existingInstanceSelector;
    SEL existingClassSelector;
    const char * existingInstanceSelectorName;
    const char * existingClassSelectorName;
    const char * nonExistingSelectorName;
    Class selectorClass;
}

-(void) setUp;

-(void) test_selectorForString_should_findExistingSelector;
-(void) test_selectorForString_should_notFindNonExistingSelector;
-(void) test_classForMetaclass_should_handleNil;
-(void) test_classForMetaclass_should_handleClass;
-(void) test_classForMetaclass_should_handleMetaClass;
-(void) test_classForID_should_handleNil;
-(void) test_classForID_should_handleInstace;
-(void) test_classForID_should_handleClass;
-(void) test_classForID_should_handleMetaclass;

@end
