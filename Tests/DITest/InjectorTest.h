/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <SenTestingKit/SenTestingKit.h>
#import "Injector.h"

@interface InjectorTest : SenTestCase {
    Injector* inj;
    
}

-(void) setUp;
-(void) tearDown;

-(void) test_create_shouldCreateRegisteredImplementation;
-(void) test_add_shouldAddNewRegistration;
-(void) test_registered_shouldFindRegisteredClass;
-(void) test_isRegistered_shouldWorkForEmptyInjector;

@end
