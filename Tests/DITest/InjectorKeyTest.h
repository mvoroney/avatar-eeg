/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <SenTestingKit/SenTestingKit.h>

@class InjectorKey;

@interface InjectorKeyTest : SenTestCase {
    InjectorKey * key1;
    InjectorKey * key2;
    InjectorKey * key3;
}

-(void) setUp;
-(void) tearDown;

-(void) test_initWith_Creates_ValidKey;
-(void) test_keyWith_Creates_ValidKey;
-(void) test_isEqual_YES_For_EqualKeys;
-(void) test_isEqual_NO_For_InEqualKeys;
-(void) test_hash_Handles_NilContext;
-(void) test_hash_Uniqueness;
-(void) test_copyWithZone_Copies;

@end
