/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
//
//  InjectorTest.m
//  di-test
//
//  Created by Testing on 13-03-27.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "InjectorTest.h"
#import "InjectorFactory.h"
#import "Injector.h"
#import "Iface.h"
#import "Iface2.h"
#import "IImpl.h"
#import "Iiimpl2.h"

#import "utils.h"

@implementation InjectorTest

-(void) setUp {
    inj = [[Injector alloc] init];
    InjectorFactory * factory = [inj registerClass:[Iface class]impl:[IImpl class]inContext: nil];
    [factory addStep: selectorForString("init", [IImpl class]) withCount: 0];
    [factory setConfigured];
}

-(void) tearDown {
    [inj release];
}

-(void) test_create_shouldCreateRegisteredImplementation {
    
    id object = [inj newObject: [Iface class] inContext: nil];
    
    STAssertNotNil(object, @"object is nil");
    STAssertTrue([object isKindOfClass:[IImpl class]], @"object is wrong type: expected IImpl, got: %@.", [object class]);
    STAssertTrue([object isKindOfClass:[Iface class]], @"object is wrong type: expected Iface, got: %@.", [object class]);
    
}

-(void) test_add_shouldAddNewRegistration {
    [inj registerClass:[Iface2 class]impl:[Iiimpl2 class]inContext: nil];
    
    STAssertTrue([inj isRegistered:[Iface2 class] inContext:nil], @"object was not found.");
}

-(void) test_registered_shouldFindRegisteredClass {
 
    STAssertFalse([inj isRegistered:[IImpl class] inContext:nil], @"object should not be found.");
}

-(void) test_isRegistered_shouldWorkForEmptyInjector {
    Injector * i = [[Injector alloc] init];
    
    STAssertFalseNoThrow([i isRegistered:[IImpl class] inContext:nil], @"object should not be found.");
    [i release];
}


@end
