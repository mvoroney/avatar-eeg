/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
//
//  InjectorFactory.m
//  plist-test
//
//  Created by Testing on 13-03-25.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "InjectorFactory.h"
#import "InjectorFactoryTest.h"

#include "utils.h"

@interface FakeResolver : NSObject {
}

-(id) newPart:(id) dep inContext:(Class) context;

@end

@implementation FakeResolver

-(id) newPart:(id) dep inContext:(Class) context {
    (void)context;
    return dep;
}

@end

@implementation InjectorFactoryTest

-(void) setUp {
    resolver = [[FakeResolver alloc] init];
    initializer = selectorForString("init:", [InjectorFactory class]);
    factory = [[InjectorFactory alloc] init: [InjectorFactory class]];
}

-(void) tearDown {
    [factory release];
}

-(void) test_initWith_Initializes {
    STAssertTrue(NULL != initializer, @"initializer should not be nil. got: %@", initializer);
    
    STAssertNotNil(factory, @"factory should be initialized.");
    STAssertTrue([factory isKindOfClass: [InjectorFactory class]], @"InjectorFactory expected, got: %@.", [factory class]);
}

-(void) test_create_CreatesCorrectObject {
    NSArray * params = [NSArray arrayWithObjects: [InjectorFactory class], nil];
    
    [factory addStep: initializer withParams: params];
    // \todo this is probably not right.
    /*
    [factory 
     addStep: selectorForString("addStep:withParams", [InjectorFactory class]) 
     withCount: 2, [NSValue value: &initializer withObjCType: @encode(SEL)], [NSArray arrayWithObjects: [InjectorFactory class], nil]];
    */
    [factory setConfigured];

    id object;
    
    object = [factory newProduct: resolver inContext: nil];
    [object addStep: initializer withParams: params];
    [object setConfigured];
    
    // because of the way we've set this up, it should be possible to repeat indefinitely.
    for (int i = 0; i < 10; i++) {

        STAssertNotNil(object, @"factory should create object, got: %@", object);
        STAssertTrue([object isKindOfClass: [InjectorFactory class]], @"InjectorFactory expected, got: %@.", [object class]);
        
        id temp = [object newProduct: resolver inContext: nil];
        [temp addStep: initializer withParams: params];
        [temp setConfigured];

        [object release];
        object = temp;
    }
    
    [object release];
}

@end
