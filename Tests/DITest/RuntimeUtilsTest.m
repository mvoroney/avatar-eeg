/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
//
//  RuntimeUtilsTest.m
//  avatar-eeg-framework
//
//  Created by Testing on 13-03-31.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "utils.h"
#import <Foundation/NSObject.h>
#import "RuntimeUtilsTest.h"

@interface TestClass : NSObject {
    
}

-(void) aGreatInstanceMethod;
+(void) aGreatClassMethod;

@end

@implementation TestClass

-(void) aGreatInstanceMethod {}
+(void) aGreatClassMethod {}

@end


@implementation RuntimeUtilsTest

-(void) setUp {
    selectorClass = [TestClass class];
    existingInstanceSelectorName = "aGreatInstanceMethod";
    existingClassSelectorName = "aGreatClassMethod";
    existingInstanceSelector = @selector(aGreatInstanceMethod);
    existingClassSelector = @selector(aGreatClassMethod);
    nonExistingSelectorName = "foobazBifaroo";
}

-(void) test_selectorForString_should_findExistingSelector {
    SEL instanceResult = selectorForString(existingInstanceSelectorName, selectorClass);
    SEL classResult = selectorForString(existingClassSelectorName, object_getClass(selectorClass));
    
    STAssertTrue(NULL != instanceResult, @"Did not find instance selector: %s, got 'NULL' instead.", existingInstanceSelector);
    STAssertEquals(existingInstanceSelector, instanceResult, @"Did not find the right selector.");
    
    STAssertTrue(NULL != classResult, @"Did not find class selector: %s, got 'NULL' instead.", existingClassSelector);
    STAssertEquals(existingClassSelector, classResult, @"Did not find the right selector.");
}  

-(void) test_selectorForString_should_notFindNonExistingSelector {
    SEL result = selectorForString(nonExistingSelectorName, selectorClass);
    
    STAssertTrue(NULL == result, @"Expected 'NULL', got: 0x%x, name: %@.", result, NSStringFromSelector(result));
}

-(void) test_classForMetaclass_should_handleNil {
    STAssertTrueNoThrow(nil == classForMetaclass(nil), @"Did not handle nil input.");
}

-(void) test_classForMetaclass_should_handleClass {
    Class result = classForMetaclass(selectorClass);
    
    STAssertNotNil(result, @"Class for: %@ should be: %@.", result, selectorClass);
    STAssertEquals(selectorClass, result, @"Wrong class? should just parot the input if it was a class.");
}

-(void) test_classForMetaclass_should_handleMetaClass {
    Class result = classForMetaclass(object_getClass(selectorClass));
    
    STAssertNotNil(result, @"Class for: %@ should be: %@.", result, selectorClass);
    STAssertEquals(selectorClass, result, @"Wrong class? expected: %@.", selectorClass);
}

-(void) test_classForID_should_handleNil {
    Class result = classForID(nil);
    
    STAssertNil(result, @"class of nil should be nil.");
}

-(void) test_classForID_should_handleInstace {
    id temp = [[selectorClass alloc] init];
    Class result = classForID(temp);
    
    STAssertNotNil(result, @"expected a valid class, got 'nil'.");
    STAssertEquals(selectorClass, result, @"Wrong class? expected: %@.", selectorClass);
    
    [temp release];
}

-(void) test_classForID_should_handleClass {
    Class result = classForID(selectorClass);
    
    STAssertNotNil(result, @"expected a valid class, got 'nil'.");
    STAssertEquals(selectorClass, result, @"Wrong class? expected: %@.", selectorClass);
}

-(void) test_classForID_should_handleMetaclass {
    Class result = classForID(object_getClass(selectorClass));
    
    STAssertNotNil(result, @"expected a valid class, got 'nil'.");
    STAssertEquals(selectorClass, result, @"Wrong class? expected: %@.", selectorClass);
}

@end
