/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <SenTestingKit/SenTestingKit.h>

@class Injector;

@interface InjectorFactoryTest : SenTestCase {
    Injector * resolver;
    id factory;
    SEL initializer;
}

-(void) setUp;
-(void) tearDown;

-(void) test_initWith_Initializes;
-(void) test_create_CreatesCorrectObject;

@end
