/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <SenTestingKit/SenTestingKit.h>
#import "ERPDeviceManager.h"

@interface ERPDeviceManager_compactListTest : SenTestCase {
    ERPDeviceList devices;
    NSUInteger listCapacity;
}

-(void) setUp;
-(void) tearDown;

-(void) test_compactList_compactsEmptyList;
-(void) test_compactList_compactsSparseList;
-(void) test_compactList_preservesDenseList;
-(void) test_compactList_worksWithRepeatedCalls;

@end
