/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
#import <Foundation/NSObject.h>
#import "ERPBuffer.h"

typedef ERPBuffer** ERPBufferList;


/**
 Manages a fixed pool of fixed size in memory buffers, logging the oldest data to disk.
 Also allows data to be queried by keyframe attribute?
 */
@interface ERPBufferPool : NSObject {
    NSUInteger nBuffers;
    ERPBufferList bufferList;
    
    // the number of frames between frames with timing info.
    NSUInteger timingInterval;
    ERPBuffer * timingInfo;
}

-(id) init;
-(id) init:(NSUInteger) bufferSize withCount:(NSUInteger) count;

-(id) queryData:(NSUInteger) index;

@property (readonly, assign) NSUInteger poolSize;

@end
