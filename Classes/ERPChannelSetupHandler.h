/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSObject.h>
#import <IOKit/IOReturn.h>

#import "ERPDeviceManager.h"
#import "ERPDataPushListener.h"

@class IOBluetoothDevice;

/**
 Uses notifications to set up connections to device services asynchronously.
 
 */
@interface ERPChannelSetupHandler : NSObject {
    ERPDevice* connectingDevice;
    id<ERPDataPushListener> dataListener;
    id notifyWhenDone;
}

-(id) init;
-(id) initWithDevice:(IOBluetoothDevice*) initDevice andDelegate:(id<ERPDataPushListener>) delegate andNotify:(id) notify;
-(void) dealloc;

-(void) rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error;
-(void) sdpQueryComplete:(IOBluetoothDevice*) device status: (IOReturn) status;
-(void) connectionComplete:(IOBluetoothDevice *) device status:(IOReturn) status;
    

@end
