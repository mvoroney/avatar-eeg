/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
/**
 @file ERPDeviceManager.h
 
 Contains ERPDeviceManager class and related protocols.
 
 @author Matt Voroney
 @author Kyle Link
 */

#import <IOKit/IOReturn.h>
#import <IOBluetooth/objc/IOBluetoothDevice.h>


@class NSString;
@class IOBluetoothDevice;
@class IOBluetoothRFCOMMChannel;
@class NSArray;

@protocol ERPDataPushListener;

struct BluetoothDeviceAddress;

typedef IOBluetoothDevice ERPDevice;
typedef ERPDevice** ERPDeviceList;
typedef IOBluetoothRFCOMMChannel ERPChannel;
typedef ERPChannel** ERPChannelList;

/**
 Object that will be used to hold and control a set of Avatar EEG Devices.
 */
@interface ERPDeviceManager : NSObject {

    @private
    NSUInteger listCapacity;
    ERPDeviceList deviceList;
    id<ERPDataPushListener>* listenerList;
    Class deviceFactory;
}

/**
 @retuns the initialized object
 */
-(id) init;

/**
 Designated initializer
 
 @param factory - A class object to be used for the construction of device
    objects internally.
 */
-(id) initWithDeviceFactory:(Class) factory;

/**
 Adds a device to be managed along with an associated data listener that will be
 registered with the system.
 
 @param address - The address of the device to be managed by the system.
 
 @param delegate - The data listener object that will be used to send 
    notifications of events such as when data arrives, or when a buffer fills,
    etc.
 
 @return the device for the address that was requested.
 */
-(ERPDevice*) addDevice:(const BluetoothDeviceAddress*) address withDelegate:(id<ERPDataPushListener>) delegate;

/**
 Removes a device from the system. The associated ERPDataListenerDelegate will
 also be unregistered. Any existing connections to the device will be closed, and
 the device will be returned to the caller for further handling.
 
 @param wantaddr - the address of the device to be removed.
 
 @return The removed device, or nil if no device matched wantaddr.
 */
-(ERPDevice*) removeDevice:(const BluetoothDeviceAddress*) wantaddr;

/**
 TODO: make this a property
 
 @return The number of devices currently being managed.
 */
-(NSUInteger) count;

/**
 Enumerates the list of devices being managed and initiates a connection to each device.
 Each connection is started asynchronously, so the return value only reflects the
 status of the connection initiation, but not whether the connection itself 
 succeeded or failed.
 
 @return kIOReturnSuccess so long as all connections were successfully initiated.
 @return The error status of the first failing device.
 
 @todo this method should probably accept a delegate so that the caller can be
    notified when each device connection is actually completed.
 
 @see connectionComplete:status: This method will be invoked when the connection is 
    actually complete.
 */
-(IOReturn) startDevices;

#pragma mark query methods
#pragma mark -
//---------------------------------------------------------------------------//
/**
 @return The list of all devices currently being managed.
 */
-(NSArray*) queryDevices;

/**
 Builds a list of all of the devices for which the given selector returns YES.
 
 @param selector - A 0 arg selector returning BOOL to invoke on each device object.
 @return An array of all the devices for which selector returned YES.
 */
-(NSArray*) queryDevices:(SEL) selector;

/**
 Builds a list of all of the devices for which the given selector return value
 matches the given attribute.
 
 @param selector - A 0 arg selector returning anything other than a floating
    point number
 
 @param attribute - An object or a value to match against the return value of
    the supplied selector. If selector returns a primitive type, then attribute
    must be wrapped in an NSValue.
 
 @return An array of all of the devices for which attribute and the return value
    of selector were considered to be equal. Objects are compared using isEqual,
    Values are compared using isEqualToValue.
 
 @todo perform type checking to make sure that the type encoding of NSValue attributes
    matches the return type of the supplied selector?
 */
-(NSArray*) queryDevices:(SEL) selector equalTo:(id) attribute;
// methods to configure devices? baud rate, etc or just do this on the device directly?

#pragma mark notification methods 
#pragma mark -
//---------------------------------------------------------------------------//

/**
 Callback that will be called by the system when the connection was actually completed.
 Currently, this method simply logs any failures.
 
 @param device - The device for which the notification pertains to.
 
 @param status - The status of the connection attempt made by the device.
 
 @see [IOBluetoothDevice-openConnection:](http://developer.apple.com/library/mac/#documentation/IOBluetooth/Reference/IOBluetoothDevice_reference/translated_content/IOBluetoothDevice.html#//apple_ref/doc/uid/TP40011421-CLSCHIOBluetoothDevice-DontLinkElementID_20)
 @see startDevices - The selector that initiates the connection process.
 
 @todo Should probably just declare this class as implementing the 
    IOBluetoothDeviceAsyncCallbacks protocol, rather than listing these methods here
    explicitly.
 */
-(void) connectionComplete:(IOBluetoothDevice *) device status:(IOReturn) status;
@end

/**
 Apple has done the annoying thing of renaming their methods between versions. One name
 exists in the older versions (i.e. withAddress), and the other name exists in newer 
 versions (i.e. deviceWithAddress).
 
 
 @todo compatibility layer. remove after OS X 10.7
 
 @todo implement this using method forwarding instead. implement doesNotRespondToSelector.
 */
@interface IOBluetoothDevice (CompatibilityAdapter)

/**
 simply delegates to withAddress.
 
 @param address - The address of the device to instantiate.
 
 see IOBluetoothDevice+withAddress on the Apple Developer Network.
 */
+(IOBluetoothDevice*) deviceWithAddressBridge: (const BluetoothDeviceAddress*) address;
-(NSArray*) servicesBridge;

@end

#define DEFAULT_DEVICE_LIST_SIZE 10ul
#define DEFAULT_DEVICE_LIST_INCREMENT 5ul
