/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>
#import <IOBluetooth/objc/IOBluetoothDevice.h>
#import <IOBluetooth/objc/IOBluetoothSDPServiceRecord.h>
#import <IOBluetooth/IOBluetoothUtilities.h>

#import <objc/objc-runtime.h>

#import <assert.h>
#import <stdlib.h> // malloc
#import <errno.h> // errno

#import "ERPDeviceManager.h"
#import "ERPDataPushListener.h"

id* firstEmptySlot(id** listRef, NSUInteger * listSize, NSUInteger increment);
NSUInteger compactList(id* list, NSUInteger size);
NSString* describeList(id* list, NSUInteger size);

@protocol IOBluetoothDeviceDeprecatedMethods
+(IOBluetoothDevice*) withAddress: (const BluetoothDeviceAddress*) address;
-(NSArray*) getServices;
@end

@implementation IOBluetoothDevice (CompatibilityAdapter)

+(IOBluetoothDevice*) deviceWithAddressBridge: (const BluetoothDeviceAddress*) address {
    ERPDevice* result = nil;
    
    // for compatibility. older versions use withAddress:, but newer versions
    // use deviceWithAddress:
    if ([self respondsToSelector: @selector(deviceWithAddress:)]) {
        result = [self deviceWithAddress: address];
    } else if ([self respondsToSelector: @selector(withAddress:)]) {
        id<IOBluetoothDeviceDeprecatedMethods> temp = (id)self;
        result = [temp withAddress: address];
    }
    
    return result;
}

-(NSArray*) servicesBridge {
    NSArray* result = nil;
    
    if ([self respondsToSelector: @selector(services)]) {
        result = self.services;
    } else if ([self respondsToSelector: @selector(getServices)]) {
        id<IOBluetoothDeviceDeprecatedMethods> temp = (id)self;
        result = [temp getServices];
    }
    
    return result;
}

@end

@implementation ERPDeviceManager


-(id) init {
    // default factory
    return [self initWithDeviceFactory: [ERPDevice class]];
}

-(id) initWithDeviceFactory:(Class) factory {
    self = [super init];
    
    if (nil != self) {
        listCapacity = DEFAULT_DEVICE_LIST_SIZE;
        deviceFactory = factory;
        
        // use nil initialized lists to distinguish empty from occupied slots.
        deviceList = (ERPDeviceList)calloc(listCapacity, sizeof(ERPDevice*));
        listenerList = (id<ERPDataPushListener>*)calloc(listCapacity, sizeof(id<ERPDataPushListener>));
        
        // handle allocation failures.
        if (!(deviceList && listenerList)) {
            NSLog(@"%d: ERPDeviceManager-init: failed to allocate buffers.", __LINE__);
            [self release];
            self = nil;
        }
    }
    
    return self;
}

-(void) dealloc {
    NSLog(@"%d: deallocating: ERPDeviceManager with deviceList: %p. listenerList: %p.", __LINE__, deviceList, listenerList);
    
    for (NSUInteger i = 0; i < listCapacity; i++) {
        
        // if any of the arrays are NULL, then we are recovering from a failed init.
        if (deviceList && listenerList) {
            assert((!(deviceList[i] || listenerList[i]) || (deviceList[i] && listenerList[i]))
                   && "deviceList and listenerList should be parallel.");
        }
            
        if (NULL != deviceList) {
            if ([deviceList[i] isConnected]) {
                IOReturn result = [deviceList[i] closeConnection];
                if (kIOReturnSuccess != result)
                    NSLog(@"%d: %s:dealloc:closeConnection on: %@ failed: reason=%x.", __LINE__, __FILE__, deviceList[i], result);
            }
            
            deviceList[i] = nil;
        }
        
        if (NULL != listenerList) {
            [listenerList[i] release];
            listenerList[i] = nil;
        }
    }
    
    if (NULL != deviceList) free(deviceList);
    if (NULL != listenerList) free(listenerList);
    deviceList = NULL;
    listenerList = NULL;
    listCapacity = 0;
    
    [super dealloc];
}

-(ERPDevice*) addDevice:(const BluetoothDeviceAddress*) address withDelegate:(id<ERPDataPushListener>) delegate {
    
    [self retain];
    NSUInteger listenerListCapacity = listCapacity;
    ERPDeviceList deviceSlot = firstEmptySlot(&deviceList, &listCapacity, DEFAULT_DEVICE_LIST_INCREMENT);
    id* listenerSlot = firstEmptySlot(&listenerList, &listenerListCapacity, DEFAULT_DEVICE_LIST_INCREMENT);
    
    if (NULL == deviceSlot || NULL == listenerSlot) {
        [NSException 
         raise: @"OutOfMemoryException" 
         format: @"ERPDevice-addDevice:withDelegate: %d: could not grow device list or listener list: errno=%d", __LINE__, errno];
    }
    
    *deviceSlot = [deviceFactory deviceWithAddress: address];
    *listenerSlot = [delegate retain];
    [self release];
    
    return *deviceSlot;
}

-(ERPDevice*) removeDevice:(const BluetoothDeviceAddress*) wantaddr {
    ERPDevice* result = nil;
    
    if (NULL == wantaddr) return nil;
    
    [self retain];
    ERPDeviceList start = deviceList;
    ERPDeviceList end = start + listCapacity;
    
    while (start < end) {
        const BluetoothDeviceAddress * gotaddr = NULL;
        if (nil != *start) {
            gotaddr = [*start getAddress];
            assert(NULL != gotaddr);
        }
        
        if (NULL != gotaddr && 0 == memcmp(wantaddr, gotaddr, sizeof(BluetoothDeviceAddress))) {
            // remove device
            result = *start;
            *start = nil;
            
            if ([result isConnected]) {
                /// \todo make this call asynchronous.
                IOReturn r = [result closeConnection];
                if (kIOReturnSuccess != r) {
                    NSLog(@"%d: %s:removeDevice:closeConnection on: %@ failed: reason=0x%x.", __LINE__, __FILE__, result, r);
                }
            }
            
            // remove listener
            /// \todo will this work if the connection is closed asynchronously?
            id* listener = listenerList + (start - deviceList);
            [*listener release];
            *listener = nil;
            
            goto search_complete;
        }
        
        start++;
    }
    
search_complete:
    [self release];
    
    return result;
}

-(NSUInteger) count {
    NSUInteger result = 0;
    
    [self retain];
    ERPDeviceList start = deviceList;
    ERPDeviceList end = start + listCapacity;
    
    while (start < end) {
        if (nil != *start++) result++;
    }
    [self release];
    
    return result;
}

/// \todo slated for removal.
-(void) connectionComplete:(IOBluetoothDevice *) device status:(IOReturn) status {
    
    if (kIOReturnSuccess != status) {
        NSLog(@"%d: %s: connectionComplete: connection to %@ failed: reason: 0x%x", __LINE__, __FILE__, device, status);
    } else {
        NSLog(@"%d: connectionComplete: baseband connection established: %@ status: %d", __LINE__, device, status);
    }
    
    [self release];
}

-(IOReturn) startDevices {
    [self retain];
    ERPDeviceList start = deviceList;
    ERPDeviceList end = start + listCapacity;
    
    IOReturn result = kIOReturnSuccess;
    while (start < end) {
        if (nil != *start) {
            /// \todo connect data delegate here?
            // initiate, pending notification via connectionComplete:status:
            
            [self retain]; // make sure we're around for notification to arive.
            IOReturn temp = [*start openConnection: self];
            
            if (kIOReturnSuccess == result) {
                // record first failure.
                result = temp;
            }
        }
        start++;
    }
    [self release];
    
    return result;
}

-(NSArray*) queryDevices {
    //NSUInteger expectedCount = [self count];
    
    [self retain];
    NSUInteger nDevices = compactList(deviceList, listCapacity);
    NSUInteger nListeners = compactList(listenerList, listCapacity);
    assert(nDevices == nListeners && "internal list inconsistency");
    
    // note: objects retained automatically on addition to array.
    NSArray * devices = [NSArray arrayWithObjects: deviceList count: nDevices];
    [self release];
    
    return devices;    
}

-(NSArray*) queryDevices:(SEL) selector {
    ERPDevice* elements[listCapacity];
    NSUInteger nElements = 0;
    NSArray* devices;
    
    [self retain];
    ERPDeviceList start = deviceList;
    ERPDeviceList end = start + listCapacity;
    
    BOOL bInclude = YES;
    
    while (start < end) {
        if (nil != *start && [*start respondsToSelector: selector]) {
            
            // NB: need to use objc_msgSend voodoo to avoid confusing OCMock.
            /// \todo why can objc_msgSend not be captured as a function pointer?
            bInclude = ((BOOL(*)(id, SEL))objc_msgSend)(*start, selector);
            if (bInclude) {
                elements[nElements++] = *start;
            }
        }
        start++;
    }
    [self release];
    
    devices = [NSArray arrayWithObjects: elements count: nElements];
    return devices;
}


/*
 * Note: this should work for eveything except possibly floating point values,
 *       depending on how NSValues storing floats compare.
 */
-(NSArray*) queryDevices:(SEL) selector equalTo:(id) attribute {
    ERPDevice* elements[listCapacity];
    NSUInteger nElements = 0;
    NSArray* devices;
    
    [self retain];
    ERPDeviceList start = deviceList;
    ERPDeviceList end = start + listCapacity;
    
    id gotAttr = nil;
    
    while (start < end) {
        if (nil != *start && [*start respondsToSelector: selector]) {
            
            // NB: need to use objc_msgSend voodoo to avoid confusing OCMock.
            /// \todo why can objc_msgSend not be captured as a function pointer?

            if ([attribute isKindOfClass: [NSValue class]]) {
                void * rawAttr = ((void*(*)(id, SEL))objc_msgSend)(*start, selector);
                gotAttr = [NSValue valueWithBytes: &rawAttr objCType: [attribute objCType]];
                
                if ([attribute isEqualToValue: gotAttr]) {
                    elements[nElements++] = *start;
                }
            } else {
                gotAttr = ((id(*)(id, SEL))objc_msgSend)(*start, selector);
                
                if ([attribute isEqual: gotAttr]) {
                    elements[nElements++] = *start;
                }
            }
        }
        start++;
    }
    [self release];
    
    devices = [NSArray arrayWithObjects: elements count: nElements];
    return devices;    
}

@end

id* firstEmptySlot(id** listRef, NSUInteger * listSize, NSUInteger increment) {
    id* result = NULL; // not found
    id* start = *listRef;
    id* end = start + *listSize;
    
    while (start < end) {
        if (nil == *start) {
            result = start;
            goto search_complete;
        }
        start++;
    }
    
    // no available slots, so grow the buffer.
    id* temp = realloc(*listRef, (*listSize + increment) * sizeof(id));
    if (NULL != temp) {
        // zero new memory
        memset(temp + *listSize, 0, increment * sizeof(id));
        
        // first available slot is now the beginning of the new region.
        result = temp + *listSize;
        
        // update out parameters.
        *listSize += increment;
        *listRef = temp;
    }
    
search_complete:
    return result;
}

/**
 Compacts the supplied list so that all of the elements will be densely packed
 towards the beginning of the list.
 */
NSUInteger compactList(id* list, NSUInteger size) {
    id* targetSlot = list;
    id* emptySlot = list;
    id* end = list + size;
    
    if (1) it_again_sam: {
        
        // find first empty slot.
        while (emptySlot < end && nil != *emptySlot) emptySlot++;
        // find the next hole.
        targetSlot = emptySlot;
        while (targetSlot < end && nil == *targetSlot) targetSlot++;
        
        // if there are any elements to shuffle down,
        // targetSlot will be less than end. 
        if (targetSlot < end) {
            // put last valid element in the hole.
            *emptySlot = *targetSlot;
            *targetSlot = nil;
            
            goto it_again_sam;
        }
    }
    
    return (NSUInteger)(emptySlot - list);
}

/**
 * Creates a string with the full contents of the supplied list, including
 * nulls, and the classname of each object.
 */
NSString* describeList(id* list, NSUInteger size) {
    id* start = list;
    id* end = list + size;
    NSMutableString * string = [NSMutableString stringWithCapacity: 100l];
    
    [string appendFormat: @"[%@:%@", [*start description], [*start class]];
    start++;
    
    while (start < end) {
        [string appendFormat: @", %@:%@", [*start description], [*start class]];
        start++;
    }
    
    [string appendString: @"]"];
    
    return string;
}
