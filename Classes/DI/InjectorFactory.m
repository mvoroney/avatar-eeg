/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSInvocation.h>
#import <Foundation/NSEnumerator.h>

#import <Foundation/NSArray.h>
#import <Foundation/NSEnumerator.h>
#import <objc/runtime.h>

#import "InjectorFactory.h"
#import "Injector.h" // temporary

#include "utils.h"

@implementation InjectorFactory

//@synthesize implementation;

-(id) init:(Class) cls {
    if (nil == cls) {
        [NSException 
         raise: @"InvalidArgumentException" 
         format: @"expected a valid class object, got: %@.", cls];
    }
    
    self = [super init];
    
    if (nil != self) {
        implementation = cls;
        configured = NO;
        
        // attempt autoconfigure.
        SEL init_requirements = selectorForString("initialization_requirements", implementation);
        if (nil != init_requirements) {
            requirements = [implementation performSelector: init_requirements];
            configured = YES;
        } else {
            requirements = [[NSMutableArray alloc] init];
        }
    }
    
    NSLog(@"%d: impl: %@.", __LINE__, cls);
        
    return self;
}

-(id) newProduct:(Injector*) resolver inContext:(Class) context {
    if (! configured) {
        [NSException 
         raise: @"InvalidOperationException" 
         format: @"Attempted to call create on an %@ that has not been configured.", [self class]];
    }
    
    NSLog(@"%d: entering InjectorFactory::create.", __LINE__);
    
    if (0 == [requirements count]) {
        // implementation has no requirements, so assume the default initializer.
        return [[implementation alloc] init];
    }
    
    NSLog(@"%d: InjectorFactory::create.", __LINE__);
    
    id result = implementation;
    NSArray * step = nil;
    NSEnumerator * stepEnum = [requirements objectEnumerator];
    while (step = [stepEnum nextObject]) {
        NSEnumerator * params = [step objectEnumerator];
        
        NSLog(@"%d: InjectorFactory::create. params: %@", __LINE__, step);
        // first index expected to be selector to perform.
        NSValue * temp = [params nextObject];
        SEL selector = [temp pointerValue];
        result = [self performStep: selector withObject: result inContext: context withResolver: resolver withParams: params];
    }
    
    NSLog(@"%d: exiting InjectorFactory::create.", __LINE__);
    
    return [result retain];
}
    
-(id) performStep:(SEL) selector withObject:(id) factory inContext:(Class) context withResolver:(id) resolver withParams:(NSEnumerator*) params {

    NSLog(@"%d: enter InjectorFactory::performStep selector: %@, impl: %@, create: %@, resolver: %@, params %@", __LINE__, NSStringFromSelector(selector), factory, implementation, resolver, params);
    
    NSMethodSignature *signature = nil;
    Class implClass = classForID(factory);
    id myFactory = nil;
    
    if ((signature = [implClass instanceMethodSignatureForSelector: selector])) {
        myFactory = (isClass(factory) ? [factory alloc] : factory);
    } else if ((signature = [implClass methodSignatureForSelector: selector])) {
        myFactory = implClass;
    } else {
        /// \todo exceptions should probably be converted to NSError, and factory cleaned up.
        NSLog(@"%d: InjectorFactory::performStep. exception...", __LINE__);
        [NSException
         raise: @"SignatureNotFoundException" 
         format: @"could not get signature for method %@ in class %@.", NSStringFromSelector(selector), implClass];
    }
    
    NSLog(@"%d: InjectorFactory::performStep. signature: %@, nArgs: %lu.", __LINE__, signature, [signature numberOfArguments]);
    
    NSLog(@"%d: InjectorFactory::performStep.", __LINE__);
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature: signature];
    NSLog(@"%d: InjectorFactory::performStep.", __LINE__);
    if (nil == invocation) {
        [NSException
         raise: @"InvocationNotPossibleException" 
         format: @"could not get invocation for method %@ with signature %@ in class %@.", 
            NSStringFromSelector(selector), signature, implClass];
    }
    
    id result = nil;
    [invocation setSelector: selector];
    [invocation setTarget: myFactory];
    NSLog(@"%d: InjectorFactory::performStep.", __LINE__);
    
    int i = 2; // The first two arguments are the hidden arguments self and _cmd
    id arg;
    while (arg = [params nextObject]) {

        NSLog(@"%d: InjectorFactory::performStep. arg: %@", __LINE__, arg);
        id resolved = [resolver newPart: arg inContext: context];
        NSLog(@"%d: InjectorFactory::performStep.", __LINE__);
        // assume that NSValue parameters should be treated as raw.
        if ([resolved isKindOfClass: [NSValue class]]) {
            NSLog(@"%d: InjectorFactory::performStep.", __LINE__);
            [invocation setArgument: [resolved pointerValue] atIndex: i++];
        } else {
            NSLog(@"%d: InjectorFactory::performStep.", __LINE__);
            [invocation setArgument: &resolved atIndex: i++];
        }
    }
    
    NSLog(@"%d: InjectorFactory::performStep: invoke: %@.", __LINE__, invocation);
    [invocation invoke]; // Invoke the selector
    [invocation getReturnValue: &result];
    
    NSLog(@"%d: InjectorFactory::performStep.", __LINE__);
    // NB: NSInvocation object should not be released.
    
    /// \todo how can we know for sure whether the factory needs to be cleaned up?
    //if (! isClass(factory) && factory != result) {
    //    [factory release];
    //}
        
    return result;
}

/// \todo is it really necessary to do so much copying...?
-(void) addStep:(SEL) selector withParams:(NSArray*) params {
    NSLog(@"%d: InjectorFactory::addStep selector: %@.", __LINE__, NSStringFromSelector(selector));
    NSMutableArray * step = [[NSMutableArray alloc] init];
    
    SEL* temp = malloc(sizeof(SEL)); /// \todo remember to free this... and is this really necessary?
    *temp = selector;
    
    NSValue * value = [NSValue value: temp withObjCType: @encode(SEL)];
    free(temp);
    [step addObject: value];
    [step addObjectsFromArray: params];
    NSLog(@"%d: InjectorFactory::addStep sel(value): %@, sel(array): %@.", __LINE__, NSStringFromSelector([value pointerValue]), NSStringFromSelector([[step objectAtIndex: 0] pointerValue]));
    [requirements addObject: step];
    [step release];
}

-(void) addStep:(SEL) selector withCount:(NSUInteger) count, ... {
    NSLog(@"%d: InjectorFactory::addStep selector: %@.", __LINE__, NSStringFromSelector(selector));
    NSMutableArray * step = [[NSMutableArray alloc] init];
    
    SEL* temp = malloc(sizeof(SEL)); /// \todo remember to free this.
    *temp = selector;
    NSValue * value = [NSValue value: temp withObjCType: @encode(SEL)];
    free(temp);
    [step addObject: value];
    
    va_list(ap);
    va_start(ap, count);
    
    for (NSUInteger i = 0; i < count; i++) {
        // do not release added objects, because we do not own them.
        [step addObject: va_arg(ap, id)];
    }
    
    va_end(ap);
    
    NSLog(@"%d: InjectorFactory::addStep selector: %@.", __LINE__, NSStringFromSelector([[step objectAtIndex: 0] pointerValue]));
    
    [requirements addObject: step];
    [step release];
}

-(BOOL) isConfigured {
    return configured;
}

-(void) setConfigured {
    configured = YES;
}

-(NSEnumerator*) stepEnumerator {
    return [requirements objectEnumerator];
}

@end
