/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
/**
 @file Injector.h
 
 Contains main dependency injector class (Injector) definition.
 
 @author Matt Voroney
 */

#import <Foundation/NSObject.h>

@class InjectorFactory;
@class NSDictionary;
@class NSMutableDictionary;

/**
 This class is the main entry point into the dependency injection framework.
 Use this class to register dependency mappings and configuration steps for
 creating fully configured instances of dependencies. This class manages a
 set of factory objects that will be responsible for creating and configuring
 mapped implementations for interface classes.
 
 Handle implementation class requirements at class level through categories.
 handle configuration requirements via global configuration object.
 
 @todo fix this comment. needs to be more accurate and complete.
 */
@interface Injector : NSObject {
    NSMutableDictionary * mappings;
}

/**
 Designated initializer.
 
 @todo This function should accept a configuration management object.
 */
-(id) init;

/**
 Instantiates a new instance of the requested class, iFace, appropriate
 to the given context.
 
 @param iFace   - The class to provide an implementation of.
 @param context - The implementation class that the iFace 
                  implementation will be used within. The primary
                  purpose of this parameter is to allow dependencies
                  to be satisfied differently for each dependent class.
                  If the dependent has no specific requirements, this
                  parameter should be set to nil, which will cause the
                  default to be used.
 
 @return A fully configured instance of the requested class iFace,
         apropriate for the specified context.
 
 @todo This function may be redundant, and newPart usable instead.
  
 @see newProduct() A similar function that seems to be strictly more
      powerful.
 */
-(id) newObject: (Class)iFace inContext: (Class)context;

/**
 Destructor.
 */
-(void) dealloc;

/**
 Register an implementation class with a class interface and context, so
 that appropriate implementation instances can be supplied for class
 interface requirements.
 
 @param iFace   - The class to register an implementation for.
 @param impl    - The class implementation to associate with iFace in the
                  supplied context.
 @param context - The context within which this association is to hold. nil
                  is the default context.
 
 @return A factory object that will be responsible for creating and
         configuring instances of the required class interface, iFace.
         The returned factory should be configured here, since there is no
         way to retrieve the factory object again in client code.
 
 @see newObject() The function that creates implementations as registered
                  by this function.
 @see InjectorFactory The class responsible for creating and configuring class
                      instances as required by dependent classes.
 */
-(InjectorFactory*) registerClass: (Class)iFace impl: (Class)impl inContext: (Class)context;

/**
 Determines whether or not the supplied class interface has an implementation
 class registered for the supplied context
 
 @param iFace  - The class interfae to check.
 @param contxt - The context to check.
 
 @return YES if iFace is registered in the given contxt, NO otherwise.
 */
-(BOOL) isRegistered: (Class)iFace inContext: (Class)contxt;

/**
 Retrieves a dependency object for some other dependent class registered with
 this dependency injector. The difference between this function and newObject(),
 is that newObject() doesn't retrieve configuration dependencies, whereas this
 one does.
 
 @todo this should probably be private, or should possibly supercede newObject().
 @todo give this function a better name (must start with 'new').
 
 @param requirement - The requested dependency to retrieve. This can be either
                      a class object or a Configurator instance.
 @param context     - The context that the part will be created in.
 
 @return The newly created part.
 
 @see newObject() This function seems strictly more powerful thatn newObject().
 */
-(id) newPart: (id)requirement inContext:(Class) context;

@end

#define INITIAL_DICTIONARY_SIZE 100ul
