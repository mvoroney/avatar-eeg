/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import "InjectorKey.h"

@implementation InjectorKey

@synthesize interface, context;

+(id) keyWith:(Class) iFace inContext: (Class)contxt {
    return [[[[self class] alloc] init: iFace inContext: contxt] autorelease];
}

-(id) init:(Class) iFace inContext: (Class)contxt {
    self = [super init];
    
    if (nil != self) {
        interface = iFace;
        context = contxt;
    }
    
    return self;
}

-(BOOL) isEqual: (id)other {
    BOOL result = NO;
    
    if ([other isKindOfClass: [self class]]) {
        InjectorKey * key = (InjectorKey*)other;
        result = (self.interface == key.interface && self.context == key.context);
    }
    
    return result;
    
}

-(NSUInteger) hash {
    // primes taken from: http://primes.utm.edu/lists/small/1000.txt
    return (INTERFACE_PRIME * (NSUInteger)self.interface ^ CONTEXT_PRIME * (NSUInteger)self.context);
}

-(id) copyWithZone: (NSZone *)zone {
    (void)zone;
    
    // since InjectorKeys are imutable we can just do a retain instead of a copy.
    NSLog(@"copyWithZone");
    return [self retain];
}


@end
