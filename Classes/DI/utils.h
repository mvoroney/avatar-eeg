/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#pragma once
#ifndef UTILS_H
#define UTILS_H

//#import <Foundation/NSObjCRuntime.h>
#import <objc/runtime.h>

SEL selectorForString(const char * name, Class cls);
Class classForMetaclass(Class metaClass);
Class classForID(id object);
BOOL isClass(id object);

#endif /* UTILS_H */
