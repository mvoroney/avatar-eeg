/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSOBject.h>

@class NSArray;
@class NSMutableArray;
@class NSEnumerator;
@class Injector;

@interface InjectorFactory: NSObject {
    Class implementation;
    //SEL initializer; // marked for removal
    NSMutableArray * requirements;
    BOOL configured;
}

//-(id) newProduct:(NSArray*) params;
-(id) init:(Class) cls;
-(id) newProduct:(Injector*) resolver inContext:(Class) context;
-(void) addStep:(SEL) selector withParams:(NSArray*) params;
-(void) addStep:(SEL) selector withCount:(NSUInteger) count, ...;
-(BOOL) isConfigured;
-(void) setConfigured;

// private
-(id) performStep:(SEL) selector withObject:(id) factory inContext:(Class) context withResolver:(id) resolver withParams:(NSEnumerator*) params;
//-(NSEnumerator*) stepEnumerator;

//@property (readonly) Class implementation;

@end
