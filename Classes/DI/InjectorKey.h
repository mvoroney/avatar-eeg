/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSObject.h>
#import <Foundation/NSZone.h>

@interface InjectorKey : NSObject <NSCopying> {
    Class interface;
    Class context;
}

+(id) keyWith:(Class) iFace inContext:(Class) contxt;
-(id) init:(Class) iFace inContext:(Class) contxt;
-(BOOL) isEqual:(id) other;
-(NSUInteger) hash;
-(id) copyWithZone:(NSZone *)zone;


@property (readonly) Class interface;
@property (readonly) Class context;

@end

enum InjectorKey_HashPrimes {
    INTERFACE_PRIME = 1019,
    CONTEXT_PRIME = 1021
};
