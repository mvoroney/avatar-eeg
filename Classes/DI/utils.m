/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#include <string.h>
#include <stdlib.h>
#include "utils.h"

SEL selectorForString(const char * csName, Class cls) {
    
    SEL implInit = NULL;
    
    while (nil != cls) {
        unsigned int nMethods = 0;
        Method * implMethods = class_copyMethodList(cls, &nMethods);
        
        /**
         * \todo is there any way to do this without a loop? this seems like too much work.
         */
        for (unsigned int i = 0; i < nMethods; i++) {
            SEL temp = method_getName(implMethods[i]);
            
            if (0 == strcmp(sel_getName(temp), csName)) {
                implInit = temp;
                free(implMethods);
                goto selector_found;
            }
        }
        
        free(implMethods);
        cls = class_getSuperclass(cls);
    }
    
selector_found:
    
    return implInit;
}

/**
 * \todo there has to be a more direct way of searching for the class for a
 *  metaclass since the association is always 1:1.
 */
Class classForMetaclass(Class metaClass) {
    if (nil == metaClass) return nil;
    
    // if metaClass was just an ordinary class, just return it.
    if (! class_isMetaClass(metaClass) 
        && class_isMetaClass(object_getClass(metaClass))) return metaClass;
        
    int nRegistered = 0;
    int nRetrieved = 0;
    Class * buffer = NULL;
    Class result = nil;
    
    // query number of registered classes.
    nRegistered = objc_getClassList(NULL, 0);
    
    if (nRegistered > 0)
        buffer = (Class*)malloc(sizeof(Class) * nRegistered);
    
    if (NULL == buffer) {
        // out of memory or 0 registered classes were found.
        return nil;
    }
    
    // retrieve registered classes.
    nRetrieved = objc_getClassList(buffer, nRegistered);
    
    if (nRetrieved != nRegistered) {
        // something went seriously wrong.
        goto class_search_complete;
    }
    
    Class * start = buffer;
    Class * end = start + nRetrieved;
    
    while (start < end) {
        result = *start++;
        
        /// \todo is this test guarnateed to work? issues with NSProxy?
        if (object_getClass(result) == metaClass) {
            // found.
            goto class_search_complete;
        }
    }
    
    // not found.
    result = nil;
    
class_search_complete:
    free(buffer);
    return result;
}

// see: http://stackoverflow.com/questions/6536244/check-if-object-is-class-type
Class classForID(id object) {
    if (nil == object) return nil;
    
    Class result = object_getClass(object);
    
    if (class_isMetaClass(result)) {
        
        // if result is a metaclass then object is a class,
        // and may also be a metaclass.
        if (class_isMetaClass(object)) {
            result = classForMetaclass(object);
        } else {
            result = object;
        }
    }
    
    return result;
}

BOOL isClass(id object) {
    if (nil == object) return NO;
    
    Class result = object_getClass(object);
    
    return (class_isMetaClass(result) ? YES : NO);
}

