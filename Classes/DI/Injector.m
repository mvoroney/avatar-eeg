/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import "Injector.h"
#import <Foundation/NSDictionary.h>
#import <Foundation/NSObject.h>

#import "InjectorKey.h"
#import "InjectorFactory.h"
#import "Configurator.h"

#include "utils.h" // flagged for removal.

@implementation Injector

/**
 * Designated initializer
 */
-(id) init {
    self = [super init];
    
    if (nil != self) {
        mappings = [[NSMutableDictionary alloc] initWithCapacity: INITIAL_DICTIONARY_SIZE];
    }
    
    return self;
}

-(id) newObject:(Class)iFace inContext:(Class)context {
    InjectorFactory * factory = [mappings objectForKey: [InjectorKey keyWith: iFace inContext: context]];

    id result = [factory newProduct: self inContext: context];
    
    return result;
}

-(id) newPart: (id) requirement inContext:(Class) context {
    
    id resolved = nil;
    
    /// \todo implement type checking of the arguments against the signature.
    /// \todo implement a configuration class. NSString is ambiguous. non-config params might be this type.
    if ([requirement isKindOfClass: [Configurator class]]) {
        // a configuration setting.
        // requirement = [[Configurator get] variable: 
    } else {
        // an interface to provide an implementation for.
        resolved = [self newObject: requirement inContext: context];
    }
    
    return resolved;
}

/**
 * \todo do we have to do any memory management on the collection key/value pairs?
 * \todo should this function take a c string instead of NSString?
 */
-(InjectorFactory*) registerClass:(Class)iFace impl:(Class)impl inContext:(Class)contxt {
    
    if (nil == impl || nil == iFace) {
        [NSException 
         raise: @"InvalidArgumentException" 
         format: @"iFace or impl was NULL."];    
    }    
    
    InjectorFactory * factory = [[InjectorFactory alloc] init: impl];
    InjectorKey * key = [[InjectorKey alloc] init: iFace inContext: contxt];
    
    [mappings setObject: factory forKey: key];
    [key release];
    [factory release];
    
    return factory;
}

-(BOOL) isRegistered:(Class) iFace inContext: (Class)contxt{
    id object_key = [InjectorKey keyWith: iFace inContext: contxt]; // keyWith:inContext:
    
    return (nil != [mappings objectForKey:object_key]);
}

-(void) dealloc {
    //releases contained objects as well.
    [mappings release];
    [super dealloc];
}

@end

