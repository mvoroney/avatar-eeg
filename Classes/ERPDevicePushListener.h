/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
#import <Foundation/NSObject.h>
#import <stdint.h>

#import "ERPDataPushListener.h"

struct ERP_ring_buffer;

@interface ERPDevicePushListener : NSObject {
    struct ERP_ring_buffer * buffer;
    id<ERPDataPushListener> dataListener;
    id notifyWhenDone;
}

-(id) init;
-(id) initWithSize:(NSUInteger) size withDelegate:(id<ERPDataPushListener>) listener andNotify:(id) notify;

-(void) dealloc;

-(void) rfcommChannelData:(IOBluetoothRFCOMMChannel*)rfcommChannel data:(void *)dataPointer length:(size_t)dataLength;
-(void) rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error;
-(void) rfcommChannelClosed:(IOBluetoothRFCOMMChannel*)rfcommChannel;
-(void) rfcommChannelControlSignalsChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel;
-(void) rfcommChannelFlowControlChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel;
-(void) rfcommChannelWriteComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel refcon:(void*)refcon status:(IOReturn)error;
-(void) rfcommChannelQueueSpaceAvailable:(IOBluetoothRFCOMMChannel*)rfcommChannel;

@property (readwrite, retain, nonatomic) id<ERPDataPushListener> dataListener;

@end

#define DEFAULT_DEVICE_PUSH_LISTENER_BUFFER_SIZE 1024ul
