/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
/// \todo include, or use predeclarations?
#import "ERPDataFormat.h"
#import <sys/time.h>

//#import <mach/mach.h>
//#import <mach/mach_time.h>

struct ERP_timing_struct_extra {
    struct ERP_timing_struct device;
    // for use with gettimeofday.
    // todo: use mach_absolute_time as well to provide nanoseconds?
    struct timeval system;
};

/**
 Data listener protocol that will be used to send notifications for various
 data events. Also allows for some tuning as to the amount of data to recieve
 and the timing granularity of data notifications.
 */
@protocol ERPDataPushListener <NSObject>

-(void) timingStructure:(struct ERP_timing_struct_extra*) buffer;
-(void) frameHeader:(struct ERP_frame_header*) header;
-(void) frameData:(struct ERP_channel*) channelData count:(NSUInteger) channelCount;

@end
