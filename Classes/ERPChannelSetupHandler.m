/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/

#import <Foundation/NSException.h>
#import <IOBluetooth/objc/IOBluetoothSDPServiceRecord.h>

#import "ERPDevicePushListener.h"
#import "ERPChannelSetupHandler.h"

@implementation ERPChannelSetupHandler

-(id) init {
    [NSException 
     raise: @"IllegalMethodException" 
     format: @"%d: ERPDeviceConnectionHandle-init: no-arg constructor is illegal for this class.", __LINE__];
    return nil;
}

-(id) initWithDevice:(IOBluetoothDevice*) initDevice andDelegate:(id<ERPDataPushListener>) delegate andNotify:(id) notify {
    
    self = [super init];
    
    if (nil != self) {
        connectingDevice = [initDevice retain];
        dataListener = [delegate retain];
        notifyWhenDone = [notify retain];
    }
    
    return self;
}

-(void) dealloc {
    [connectingDevice release];
    [dataListener release];
    [notifyWhenDone release];
    
    [super dealloc];
}

-(void) rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error {
    [notifyWhenDone rfcommChannelOpenComplete: rfcommChannel status: error];
    
    // once the channel has been established, our work here is done.
    [self release];
}

-(void) sdpQueryComplete:(IOBluetoothDevice*) device status:(IOReturn) status {
    
    if (kIOReturnSuccess == status) {
        // once the service query is complete, we can initiate an RFCOMM connection.
        
        IOReturn result;
        
        NSArray *services = [device servicesBridge];
        if ([services count] <= 0) {
            NSLog(@"%d: ERPChannelSetupHandler: no services were found for device: %@", __LINE__, device);
        } else if ([services count] > 1) {
            NSLog(@"%d: ERPChannelSetupHandler: unexpected number of services for device: %@ expected: 1, got: %ld", __LINE__, device, [services count]);
        }
        
        /// \todo search for specific service?
        IOBluetoothSDPServiceRecord *serviceRecord = [services objectAtIndex: 0];
        assert(nil != serviceRecord && "should be impossible for serviceRecord to be nil.");
        
        /// \todo how should the rfcommDelegate actually be managed? should it be
        ///     released by the ERPDeviceManager instead?
        /// \todo should this instantiation be factored out? it makes testing a bit more difficult.
        id rfcommDelegate = [[(ERPDevicePushListener*)[ERPDevicePushListener alloc] 
                              initWithSize: DEFAULT_DEVICE_PUSH_LISTENER_BUFFER_SIZE 
                              withDelegate: dataListener andNotify: self] autorelease];
        
        BluetoothRFCOMMChannelID rfChanID;
        result = [serviceRecord getRFCOMMChannelID: &rfChanID];
        assert(kIOReturnSuccess == result && "rfcomm channel id should be retrieved.");
        
        /// \todo should we do something with the channel here? or just wait to recieve it back in the deviceManager.
        IOBluetoothRFCOMMChannel *rfcommChannel;
        result = [device openRFCOMMChannelAsync: &rfcommChannel withChannelID: rfChanID delegate: rfcommDelegate];
        
        if (kIOReturnSuccess != result) {
            NSLog(@"%d: ERPChannelSetupHandler: failed to initiate RFCOMM connection for device: %@", __LINE__, device);
        }
    } else {
        NSLog(@"%d: ERPChannelSetupHandler-sdpQueryComplete: %@ had bad status: %d", __LINE__, device, status);
    }
}

-(void) connectionComplete:(IOBluetoothDevice *) device status:(IOReturn) status {
    
    if (kIOReturnSuccess != status) {
        NSLog(@"%d: ERPChannelSetupHandler-connectionComplete: connection to %@ failed: reason: 0x%x", __LINE__, device, status);
    } else {
        NSLog(@"%d: ERPChannelSetupHandler-connectionComplete: baseband connection established: %@ status: %d", __LINE__, device, status);
    }
    
    // once the baseband connection is established, we can perform the SDP query to determine available services.
    [device performSDPQuery: self];
}


@end
