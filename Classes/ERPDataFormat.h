
#ifndef ERP_DATA_FORMAT_H
#define ERP_DATA_FORMAT_H

#import <stdint.h>

// raw structures
struct ERP_timing_struct {
    uint32_t SOC;
    uint32_t counter;
    uint32_t frame_count;
    uint32_t reserved[3];
};

struct ERP_channel {
    uint8_t data[3];
};

struct ERP_channel_set {
    struct ERP_channel channels[8];
};

struct ERP_frame_header {
    uint8_t sync;
    uint8_t version;
    uint16_t framesize;
    uint8_t frametype;
    uint32_t framecount;
    uint8_t channels;
    uint16_t samples; 
};

struct ERP_frame {
    struct ERP_frame_header header;
    // 16 blocks of 8 channels each.
    /// \todo docs mark this as the 'typical' data frame size. will it always be this?
    struct ERP_channel_set data[16];
};


#endif /* ERP_DATA_FORMAT_H */
