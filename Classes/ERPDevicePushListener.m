/*
Copyright (c) 2013 Matthew Tatta, Matt Voroney, Kyle Link

See the file license.txt for copying permission.
*/
#import <IOBluetooth/objc/IOBluetoothRFCOMMChannel.h>

#import <stdlib.h>
#import <errno.h>
#import <string.h>

#import "ERPDataFormat.h"
#import "ERPDevicePushListener.h"
#import "ERPDataPushListener.h"

struct ERP_ring_buffer {
    uint8_t * buffer;
    NSUInteger length;
    NSUInteger writePos;
    NSUInteger readPos;
};

// ERP_ring_buffer functions
NSUInteger readBuffer(uint8_t* dstBuffer, const struct ERP_ring_buffer* srcBuffer, NSUInteger length);
NSUInteger writeBuffer(struct ERP_ring_buffer* dstBuffer, const uint8_t* srcBuffer, NSUInteger length);

@interface ERPDevicePushListener () <IOBluetoothRFCOMMChannelDelegate>

-(void) rfcommChannelDataDispatch;

@end

@implementation ERPDevicePushListener

@synthesize dataListener;

#pragma mark Initializers
#pragma mark -

-(id) init {
    return [self initWithSize: DEFAULT_DEVICE_PUSH_LISTENER_BUFFER_SIZE withDelegate: nil andNotify: nil];
}

-(id) initWithSize:(NSUInteger) size withDelegate:(id<ERPDataPushListener>) listener andNotify:(id) notify {
    
    self = [super init];
    if (nil != self) {
        buffer = NULL;
        buffer = malloc(sizeof(struct ERP_ring_buffer));
        
        if (NULL != buffer) {
            buffer->buffer = (uint8_t*)malloc(size);
        }
        
        if (!buffer || !buffer->buffer) {
            NSLog(@"%d: ERPDevicePushListener-init:withDelegate:: buffer allocation failed. out of memory. errno=%d", __LINE__, errno);
            [self release];
            self = nil;
        } else {
        
            dataListener = [listener retain];
            notifyWhenDone = [notify retain];
            buffer->length = size;
            buffer->writePos = 0;
            buffer->readPos = 0;
        }
    }
    
    return self;
}

-(void) dealloc {
    
    if (!buffer || !buffer->buffer) {
        NSLog(@"%d: ERPDevicePushListener-dealloc: buffer or buffer->buffer was NULL. recovering from init failure?", __LINE__);
    }
    
    if (NULL != buffer) {
        if (NULL != buffer->buffer) free(buffer->buffer);
        free(buffer);
        buffer = NULL;
    }
    
    [dataListener release];
    [notifyWhenDone release];
    
    [super dealloc];
}

-(void) rfcommChannelDataDispatch {
    
}

#pragma mark IOBluetoothRFCOMMChannelDelegate Methods
#pragma mark -

/// \todo need to rewrite this in terms of ringbuffer functions.
- (void)rfcommChannelData:(IOBluetoothRFCOMMChannel*)rfcommChannel data:(void *)dataPointer length:(size_t)dataLength {
    
    /// \todo do we need to do anything with the channel?
    (void)rfcommChannel;
    
    /// \todo get system time.
    
    if (buffer->writePos + dataLength > buffer->length) {
        uint8_t* bufTemp = realloc(buffer->buffer, buffer->writePos + dataLength);
        
        if (bufTemp == NULL) {
            NSLog(@"%d: ERPDevicePushListener-rfcommChannelData: out of memory. could not grow buffer. errno=%d", __LINE__, errno);
        } else {
            buffer->buffer = bufTemp;
            buffer->length = buffer->writePos + dataLength;
        }
        
    }
    
    // only copy data there is space for, and log failures.
    if (buffer->writePos + dataLength > buffer->length) {
        NSLog(@"%d: ERPDevicePushListener-rfcommChannelData: data loss. failed to copy %ld bytes. errno=%d",
              __LINE__, buffer->writePos + dataLength - buffer->length, errno);
        dataLength = (buffer->length - buffer->writePos);
    }
    
    /// \todo use ERP_ring_buffer routines instead.
    memcpy(buffer->buffer + buffer->writePos, dataPointer, dataLength);
    buffer->writePos += dataLength;
    
    // dispatch to parsing thread.
    [self rfcommChannelDataDispatch];
}

-(void) rfcommChannelOpenComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel status:(IOReturn)error {
    NSLog(@"%d: ERPDevicePushListener-rfcommChannelOpenComplete: %@ status: 0x%x.", __LINE__, rfcommChannel, error);
}

-(void) rfcommChannelClosed:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    NSLog(@"%d: ERPDevicePushListener-rfcommChannelClosed: %@.", __LINE__, rfcommChannel);
}

-(void) rfcommChannelControlSignalsChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    NSLog(@"%d: ERPDevicePushListener-rfcommChannelControlSignalsChanged: %@.", __LINE__, rfcommChannel);
}

-(void) rfcommChannelFlowControlChanged:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    NSLog(@"%d: ERPDevicePushListener-rfcommChannelFlowControlChanged: %@.", __LINE__, rfcommChannel);
}

-(void) rfcommChannelWriteComplete:(IOBluetoothRFCOMMChannel*)rfcommChannel refcon:(void*)refcon status:(IOReturn)error {
    NSLog(@"%d: ERPDevicePushListener-rfcommChannelWriteComplete: %@ refcon: %p status: 0x%x.", __LINE__, rfcommChannel, refcon, error);
}

-(void) rfcommChannelQueueSpaceAvailable:(IOBluetoothRFCOMMChannel*)rfcommChannel {
    NSLog(@"%d: ERPDevicePushListener-rfcommChannelQueueSpaceAvailable: %@.", __LINE__, rfcommChannel);
}

@end

NSUInteger readBuffer(uint8_t* dstBuffer, const struct ERP_ring_buffer* srcBuffer, NSUInteger length) {
    assert(length > srcBuffer->length && "should never attempt to read more data than the buffer can contain.");
    
    NSUInteger fromPos = srcBuffer->readPos % srcBuffer->length;
    NSUInteger toPos = (fromPos + length) % srcBuffer->length;
    
    NSUInteger nCopied = 0;
    
    if (fromPos >= toPos) {
        // handle buffer wraparound.
        NSUInteger backLength = srcBuffer->length - fromPos;
        NSUInteger frontLength = toPos;
        nCopied = frontLength + backLength;
        
        memcpy(dstBuffer, srcBuffer->buffer + fromPos, backLength);
        memcpy(dstBuffer + backLength, srcBuffer->buffer, frontLength);
    } else {
        
        nCopied = toPos - fromPos;
        memcpy(dstBuffer, srcBuffer->buffer + fromPos, nCopied);
    }

    return nCopied; // number of bytes copied?
}

NSUInteger writeBuffer(struct ERP_ring_buffer* dstBuffer, const uint8_t* srcBuffer, NSUInteger length) {
    return 0;
}
